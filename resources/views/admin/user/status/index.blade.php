@extends('admin.layouts.default')

@section('header')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Applicant</li>
                            <li class="breadcrumb-item active" aria-current="page">Status</li>
                        </ol>
                    </nav>
                </div>
                @if (session('statusApplicantSuccessMessage'))
                    <div class="col-lg-6 col-5">
                        <div class="alert alert-success alert-dismissible fade show p-2" role="alert">
                            <span class="alert-icon ml-3"><i class="ni ni-check-bold"></i></span> {{ session('statusApplicantSuccessMessage') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
                <div class="row">
                    <div class="col-6">
                        <h3 class="mb-0">Applicant Status List</h3>
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{ route('admin.user.status.export') }}" class="btn btn-success btn-sm">
                            <i class="fas fa-file-export"></i> Export Applicant Status List
                        </a>
                    </div>
                </div>
            </div>
            <!-- Light table -->
            <div class="table-responsive">
                <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Job</th>
                            <th scope="col">Score</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        @forelse ($applicants as $applicant)
                            <tr>
                                <td>{{ $applicant->user->name }}</td>
                                <td>{{ $applicant->job->job_name }}</td>
                                <td>{{ $applicant->test_score }}</td>
                                <td>
                                    @if ($applicant->status === 'rejected')
                                        <span class="badge badge-danger">Rejected</span>
                                    @elseif ($applicant->status === 'accepted')
                                        <span class="badge badge-success">Accepted</span>
                                    @elseif ($applicant->isHavePsychologicalTest && $applicant->isHaveSkillTest)
                                        <a href="{{ route('admin.user.status.view-test', $applicant->id) }}">View Test</a>
                                    @elseif($applicant->status === 'waiting')
                                        <a href="{{ route('admin.user.status.view-data', $applicant->id) }}">View User Data</a>
                                    @else
                                        <span class="badge badge-secondary">Waiting for Online Test</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin.user.status.edit', $applicant->id) }}" class="btn btn-warning btn-sm">
                                        <i class="fas fa-edit"></i> Edit
                                    </a>
                                    <a href="javascript:;;" class="btn btn-danger btn-sm" onclick="deleteUserModal({{ $applicant->id }})">
                                        <i class="fas fa-trash-alt"></i> Delete
                                    </a>
                                </td>
                            </tr>
                        @empty
                        <tr class="text-center">
                            <td colspan="6" class="text-muted">No data available</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- Card footer -->
            <div class="card-footer py-4">
                {{-- {{ $users->links() }} --}}
            </div>
        </div>
    </div>
</div>

{{-- Modal Delete User --}}
<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <form id="deleteUser" class="d-none" action="" method="post">
            @csrf
            @method('delete')

        </form>
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Delete User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Are you sure you want to delete this data?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-danger" onclick="$('#deleteUser').submit()">Yes</button>
        </div>
        </div>
    </div>
</div>
{{-- End Modal Delete User --}}
@endsection

@section('scripts')
<script>
    function deleteUserModal(id) {
        $('#deleteUser').attr('action', '{{url('admin/user/status/delete/')}}/'+id)
        $('#deleteUserModal').modal('show')
    }
</script>
@endsection
