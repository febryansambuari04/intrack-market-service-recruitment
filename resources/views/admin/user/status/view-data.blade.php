@extends('admin.layouts.default')

@section('header')
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item">Applicant</li>
                                <li class="breadcrumb-item">
                                    <a href="{{ route('admin.user.status.index') }}">Status</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">View User Data</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <h1 class="text-center mt-2">Applicant Data: {{ $user->name }}</h1>
                    <div class="nav-wrapper p-3">
                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 active" id="basic-info-tab" data-toggle="tab" href="#basic-info" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true">
                                    <i class="ni ni-badge mr-2"></i> Basic Info
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="education-tab" data-toggle="tab" href="#education" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false">
                                    <i class="ni ni-ruler-pencil mr-2"></i> Educations
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="experience-tab" data-toggle="tab" href="#experience" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false">
                                    <i class="ni ni-briefcase-24 mr-2"></i> Experience
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="document-tab" data-toggle="tab" href="#document" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false">
                                    <i class="ni ni-paper-diploma mr-2"></i> Documents
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="tab-content" id="myTabContent">
                                {{-- Basic Info Tab --}}
                                <div class="tab-pane fade show active" id="basic-info" role="tabpanel" aria-labelledby="basic-info-tab">
                                    <form method="post" action="#" autocomplete="off" enctype="multipart/form-data">
                                        @csrf

                                        <h6 class="heading-small text-muted mb-4">{{ __('User information') }}</h6>

                                        <div class="pl-lg-4">
                                            <div class="form-group{{ $errors->has('full_name') ? ' has-danger' : '' }}">
                                                <label class="form-control-label" for="input-full-name">{{ __('Full Name') }}</label>
                                                <input type="text" name="full_name" id="input-full-name" class="form-control form-control-alternative{{ $errors->has('full_name') ? ' is-invalid' : '' }}" placeholder="{{ __('Full Name') }}" value="{{ old('full_name', $user->full_name) }}" disabled>

                                                @if ($errors->has('full_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('full_name') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                                <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                                <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Name') }}" value="{{ old('name', $user->name) }}" disabled>

                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                                <label class="form-control-label" for="input-email">{{ __('Email') }}</label>
                                                <input type="email" name="email" id="input-email" class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" value="{{ old('email', $user->email) }}" disabled>

                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('birth_date') ? ' has-danger' : '' }}">
                                                <label class="form-control-label" for="input-birth-date">{{ __('Birth Date') }}</label>
                                                <input type="date" name="birth_date" id="input-birth-date" class="form-control form-control-alternative{{ $errors->has('birth_date') ? ' is-invalid' : '' }}" placeholder="{{ __('Birth Date') }}" value="{{ old('birth_date', $user->birth_date) }}" disabled>

                                                @if ($errors->has('birth_date'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('birth_date') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('gender') ? ' has-danger' : '' }}">
                                                <legend class="col-form-label">Gender</legend>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="gender" id="male" value="Male" {{ ($user->gender == "Male") ? 'checked' : '' }} disabled>
                                                    <label class="form-check-label" for="male">
                                                        Male
                                                    </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="gender" id="female" value="Female" {{ ($user->gender == "Female") ? 'checked' : '' }} disabled>
                                                    <label class="form-check-label" for="female">
                                                        Female
                                                    </label>
                                                </div>

                                                @if ($errors->has('gender'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('gender') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                                                <label class="form-control-label" for="input-address">Address</label>
                                                <textarea name="address" class="form-control" id="input-address" rows="3" disabled>{{ old('address', $user->address) }}</textarea>

                                                @if ($errors->has('address'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('phone_number') ? ' has-danger' : '' }}">
                                                <label class="form-control-label" for="input-phone-number">{{ __('Phone Number') }}</label>
                                                <input type="text" name="phone_number" id="input-phone-number" class="form-control form-control-alternative{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" value="{{ old('phone_number', $user->phone_number) }}" disabled>

                                                @if ($errors->has('phone_number'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('hobby') ? ' has-danger' : '' }}">
                                                <label class="form-control-label" for="input-hobby">{{ __('Hobby') }}</label>
                                                <input type="text" name="hobby" id="input-hobby" class="form-control form-control-alternative{{ $errors->has('hobby') ? ' is-invalid' : '' }}" placeholder="{{ __('Hobby') }}" value="{{ old('hobby', $user->hobby) }}" disabled>

                                                @if ($errors->has('hobby'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('hobby') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                {{-- Basic Info Tab --}}

                                {{-- Education Tab --}}
                                <div class="tab-pane fade" id="education" role="tabpanel" aria-labelledby="education-tab">
                                    <div class="row mb-4">
                                        <div class="col-md-8">
                                            <h6 class="heading-small text-muted">{{ __('User Educations') }}</h6>
                                        </div>
                                    </div>

                                    @foreach($user->educations as $education)
                                        <div class="row mb-3">
                                            <div class="col-md-2 text-center">
                                                <img src="{{ asset('assets/img/icons/user/Graduation Cap.svg') }}" width="80" alt="icon education">
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="text-muted">{{ date('Y', strtotime($education->start_period)) }} - {{ date('Y', strtotime($education->end_period)) }}</h5>
                                                <h3>{{ $education->major }} at {{ $education->school_name }}</h3>
                                                <h4>{{ $education->degree }}</h4>
                                                <p class="text-muted">{{ $education->gpa }} / {{ $education->gpa_max }}</p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                {{-- Education Tab --}}

                                {{-- Experience Tab --}}
                                <div class="tab-pane fade" id="experience" role="tabpanel" aria-labelledby="experience-tab">
                                    <div class="row mb-4">
                                        <div class="col-md-8">
                                            <h6 class="heading-small text-muted">{{ __('User Experiences') }}</h6>
                                        </div>
                                    </div>

                                    @foreach($user->experiences as $experience)
                                        <div class="row mb-3">
                                            <div class="col-md-2 text-center">
                                                <img src="{{ asset('assets/img/icons/user/Project Management.svg') }}" width="80" alt="icon experience">
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="text-muted">{{ date('Y', strtotime($experience->start_period)) }} - {{ date('Y', strtotime($experience->end_period)) }}</h5>
                                                <h3>{{ $experience->position_name }} at {{ $experience->company_name }}</h3>
                                                <p class="text-muted">{{ $experience->working_description }}</p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                {{-- Experience Tab --}}

                                {{-- Document Tab --}}
                                <div class="tab-pane fade" id="document" role="tabpanel" aria-labelledby="document-tab">
                                    <div class="row mb-4">
                                        <div class="col-md-8">
                                            <h6 class="heading-small text-muted">{{ __('User Docouments') }}</h6>
                                        </div>
                                    </div>

                                    @foreach($user->documents as $document)
                                        <div class="row mb-3">
                                            <div class="col-md-2 text-center">
                                                <img src="{{ asset('assets/img/icons/user/PDF.svg') }}" width="80" alt="icon document">
                                            </div>
                                            <div class="col-md-8">
                                                <h3>{{ $document->document_name }}</h3>
                                                <a href="/file/user/profile/{{ $document->file }}" target="_blank">View Document</a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                {{-- Document Tab --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
