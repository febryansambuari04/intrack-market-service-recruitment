@extends('admin.layouts.default')

@section('header')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Applicant</li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.user.status.index') }}">Status</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Edit</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <!-- Card header -->
            <div class="card-header border-1" style="background-color: #f6f9fc;">
                <div class="row">
                    <div class="col-6">
                        <h3 class="mb-0">Edit Status Applicant</h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('admin.user.status.update', $applicant->id) }}" autocomplete="off">
                    @csrf

                    <div class="pl-lg-4">
                        <div class="form-group">
                            <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                            <input type="text" name="name" id="input-name" class="form-control form-control-alternative" placeholder="{{ __('Name') }}" value="{{ old('name') ?? $applicant->user->name }}" readonly>
                        </div>

                        <div class="form-group">
                            <label class="form-control-label" for="input-job">{{ __('Job Name') }}</label>
                            <input type="text" name="job_name" id="input-job" class="form-control form-control-alternative" placeholder="{{ __('Job Name') }}" value="{{ old('job_name') ?? $applicant->job->job_name }}" readonly>
                        </div>

                        <div class="form-group">
                            <label class="form-control-label" for="input-score">{{ __('Test Score') }}</label>
                            <input type="text" name="test_score" id="input-score" class="form-control form-control-alternative" placeholder="{{ __('Test Score') }}" value="{{ old('test_score') ?? $applicant->test_score }}" readonly>
                        </div>

                        <div class="form-group">
                            <label class="form-control-label" for="input-status">{{ __('Status') }} <span class="text-danger">*</span></label>
                            <select class="form-control form-control-alternative{{ $errors->has('status') ? ' is-invalid' : '' }}" id="input-status" name="status">
                                <option value="">{{ __('--- Select Status ---') }}</option>
                                <option value="applied">Online Test</option>
                                <option value="accepted">Accept</option>
                                <option value="rejected">Reject</option>
                            </select>
                        </div>

                        <div class="form-group" id="zoomUrlField" hidden>
                            <label class="form-control-label" for="input-zoom-url">{{ __('Zoom URL') }}</label>
                            <input type="text" name="zoom_url" id="input-zoom-url" class="form-control form-control-alternative" placeholder="{{ __('Zoom URL') }}" value="{{ old('zoom_url') ?? $applicant->zoom_url }}">
                        </div>

                        <div class="form-group" id="descriptionField" hidden>
                            <label class="form-control-label" for="input-description">{{ __('Description') }}</label>
                            <textarea name="description" class="form-control form-control-alternative" id="input-description" rows="3" placeholder="{{ __('Description') }}">{{ old('description') }}</textarea>
                        </div>

                        <div class="text-center">
                            <a href="{{ route('admin.user.status.index') }}" class="btn btn-secondary">{{ __('Back') }}</a>
                            <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#input-status').change(function () {
        var option =  $('#input-status').find(":selected").text();
            if (option === "Accept") {
                $('#zoomUrlField').removeAttr('hidden');
                $('#descriptionField').removeAttr('hidden');
            }
        })
    });
</script>
@endsection
