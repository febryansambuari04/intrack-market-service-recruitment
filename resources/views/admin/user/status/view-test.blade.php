@extends('admin.layouts.default')

@section('header')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Applicant</li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.user.status.index') }}">Status</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">View Test</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <!-- Card header -->
            <div class="card-header border-1" style="background-color: #f6f9fc;">
                <div class="row">
                    <div class="col-6">
                        <h3 class="mb-0">Choose The Test</h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-primary btn-sm" onclick="showPsychologicalTest()">Psychological Test</button>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-primary btn-sm" onclick="showSkillTest()">Skill Test</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" id="pscyhologicalTestCard" hidden>
    <div class="col">
        <div class="card">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h3 class="mb-0">Applicant Psychological Test Result</h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-8">
                    @foreach ($psychologicalTest as $number => $test)
                        <div class="row mb-3">
                            <div class="col-8">
                                <h2 class="mb-0">{{ $number + 1 . '. ' . $test->psychologicalTest->question }}</h2>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-6">
                                <div class="custom-control custom-control-alternative custom-radio mb-3">
                                    <input name="answer_[{{ $test->psychologicalTest->id }}]" class="custom-control-input" id="{{ $test->psychologicalTest->id }}answer_1" value="{{ $test->psychologicalTest->answer_1 }}" type="radio" disabled>
                                    <label class="custom-control-label" for="{{ $test->psychologicalTest->id }}answer_1">{{ $test->psychologicalTest->answer_1 }}</label>
                                </div>
                                <div class="custom-control custom-control-alternative custom-radio mb-3">
                                    <input name="answer_[{{ $test->psychologicalTest->id }}]" class="custom-control-input" id="{{ $test->psychologicalTest->id }}answer_2" value="{{ $test->psychologicalTest->answer_2 }}" type="radio">
                                    <label class="custom-control-label" for="{{ $test->psychologicalTest->id }}answer_2">{{ $test->psychologicalTest->answer_2 }}</label>
                                </div>
                                <small class="font-weight-bold text-primary">Applicant Answer : {{ $test->answer }}</small>
                            </div>
                            <div class="col-6">
                                <div class="custom-control custom-control-alternative custom-radio mb-3">
                                    <input name="answer_[{{ $test->psychologicalTest->id }}]" class="custom-control-input" id="{{ $test->psychologicalTest->id }}answer_3" value="{{ $test->psychologicalTest->answer_3 }}" type="radio">
                                    <label class="custom-control-label" for="{{ $test->psychologicalTest->id }}answer_3">{{ $test->psychologicalTest->answer_3 }}</label>
                                </div>
                                <div class="custom-control custom-control-alternative custom-radio mb-3">
                                    <input name="answer_[{{ $test->psychologicalTest->id }}]" class="custom-control-input" id="{{ $test->psychologicalTest->id }}answer_4" value="{{ $test->psychologicalTest->answer_4 }}" type="radio">
                                    <label class="custom-control-label" for="{{ $test->psychologicalTest->id }}answer_4">{{ $test->psychologicalTest->answer_4 }}</label>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" id="skillTestCard" hidden>
    <div class="col">
        <div class="card">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h3 class="mb-0">Applicant Skill Test Result</h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-8">
                    @foreach ($skillTest as $number => $test)
                        <div class="row mb-3">
                            <div class="col-8">
                                <h2 class="mb-0">{{ $number + 1 . '. ' . $test->skillTest->question }}
                                    @if (Str::lower($test->skillTest->right_answer) == Str::lower($test->answer))
                                        <span><i class="ni ni-check-bold text-success"></i></span>
                                    @else
                                        <span><i class="ni ni-fat-remove text-danger"></i></span>
                                    @endif
                                </h2>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-6">
                                <div class="custom-control custom-control-alternative custom-radio mb-3">
                                    <input name="answer_[{{ $test->skillTest->id }}]" class="custom-control-input" id="{{ $test->skillTest->id }}answer_1" value="{{ $test->skillTest->answer_1 }}" type="radio">
                                    <label class="custom-control-label" for="{{ $test->skillTest->id }}answer_1">{{ $test->skillTest->answer_1 }}</label>
                                </div>
                                <div class="custom-control custom-control-alternative custom-radio mb-3">
                                    <input name="answer_[{{ $test->skillTest->id }}]" class="custom-control-input" id="{{ $test->skillTest->id }}answer_2" value="{{ $test->skillTest->answer_2 }}" type="radio">
                                    <label class="custom-control-label" for="{{ $test->skillTest->id }}answer_2">{{ $test->skillTest->answer_2 }}</label>
                                </div>
                                <small class="font-weight-bold text-primary">Applicant Answer : {{ $test->answer }}</small>
                            </div>
                            <div class="col-6">
                                <div class="custom-control custom-control-alternative custom-radio mb-3">
                                    <input name="answer_[{{ $test->skillTest->id }}]" class="custom-control-input" id="{{ $test->skillTest->id }}answer_3" value="{{ $test->skillTest->answer_3 }}" type="radio">
                                    <label class="custom-control-label" for="{{ $test->skillTest->id }}answer_3">{{ $test->skillTest->answer_3 }}</label>
                                </div>
                                <div class="custom-control custom-control-alternative custom-radio mb-3">
                                    <input name="answer_[{{ $test->skillTest->id }}]" class="custom-control-input" id="{{ $test->skillTest->id }}answer_4" value="{{ $test->skillTest->answer_4 }}" type="radio">
                                    <label class="custom-control-label" for="{{ $test->skillTest->id }}answer_4">{{ $test->skillTest->answer_4 }}</label>
                                </div>
                                <small class="font-weight-bold text-primary">Right Answer : {{ $test->skillTest->right_answer }}</small>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    function showPsychologicalTest() {
        if ($('#pscyhologicalTestCard').is(":hidden")) {
            $('#pscyhologicalTestCard').removeAttr('hidden');
            $('#skillTestCard').attr('hidden', true);
        } else {
            $('#pscyhologicalTestCard').attr('hidden', true);
        }
    }

    function showSkillTest() {
        if ($('#skillTestCard').is(":hidden")) {
            $('#skillTestCard').removeAttr('hidden');
            $('#pscyhologicalTestCard').attr('hidden', true);
        } else {
            $('#skillTestCard').attr('hidden', true);
        }
    }
</script>
@endsection
