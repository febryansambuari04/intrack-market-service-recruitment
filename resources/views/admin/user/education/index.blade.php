@extends('admin.layouts.default')

@section('header')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">User</li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.user.index') }}">Applicant</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Education</li>
                        </ol>
                    </nav>
                </div>
                @if (session('educationDeleteSuccess'))
                    <div class="col-lg-6 col-5">
                        <div class="alert alert-success alert-dismissible fade show p-2" role="alert">
                            <span class="alert-icon ml-3"><i class="ni ni-check-bold"></i></span> {{ session('educationDeleteSuccess') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
                <h3 class="mb-0">Education Applicant List</h3>
            </div>
            <!-- Light table -->
            <div class="table-responsive">
                <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">School / University Name</th>
                            <th scope="col">Degree</th>
                            <th scope="col">Major</th>
                            <th scope="col">GPA</th>
                            <th scope="col">Period</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        @forelse ($educations as $education)
                            <tr>
                                <td>{{ $education->school_name }}</td>
                                <td>{{ $education->degree }}</td>
                                <td>{{ $education->major }}</td>
                                <td>
                                    {{ $education->gpa }} of {{ $education->gpa_max }}
                                </td>
                                <td>
                                    {{ $education->start_period }} to {{ $education->end_period }}
                                </td>
                                <td>
                                    <a href="javascript:;;" class="btn btn-danger btn-sm" onclick="deleteEducationModal({{ $education->id }})">
                                        <i class="fas fa-trash-alt"></i> Delete
                                    </a>
                                </td>
                            </tr>
                        @empty
                        <tr class="text-center">
                            <td colspan="6" class="text-muted">No data available</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- Card footer -->
            <div class="card-footer py-4">
                {{ $educations->links() }}
            </div>
        </div>
    </div>
</div>

{{-- Modal Delete User --}}
<div class="modal fade" id="deleteEducationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <form id="deleteEducation" class="d-none" action="" method="post">
            @csrf
            @method('delete')

        </form>
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Delete Education</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Are you sure you want to delete this data?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-danger" onclick="$('#deleteEducation').submit()">Yes</button>
        </div>
        </div>
    </div>
</div>
{{-- End Modal Delete User --}}
@endsection

@section('scripts')
<script>
    function deleteEducationModal(id) {
        $('#deleteEducation').attr('action', '{{url('admin/user/education/delete/')}}/'+id)
        $('#deleteEducationModal').modal('show')
    }
</script>
@endsection
