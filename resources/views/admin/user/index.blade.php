@extends('admin.layouts.default')

@section('header')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Applicant</li>
                            <li class="breadcrumb-item active" aria-current="page">User</li>
                        </ol>
                    </nav>
                </div>
                @if (session('userDeleteSuccess'))
                    <div class="col-lg-6 col-5">
                        <div class="alert alert-success alert-dismissible fade show p-2" role="alert">
                            <span class="alert-icon ml-3"><i class="ni ni-check-bold"></i></span> {{ session('userDeleteSuccess') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
                <div class="row">
                    <div class="col-6">
                        <h3 class="mb-0">Applicant List</h3>
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{ route('admin.user.export') }}" class="btn btn-success btn-sm">
                            <i class="fas fa-file-export"></i> Export Applicant
                        </a>
                    </div>
                </div>
            </div>
            <!-- Light table -->
            <div class="table-responsive">
                <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Education</th>
                            <th scope="col">Experience</th>
                            <th scope="col">Document</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        @forelse ($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <a href="{{ route('admin.user.education.index', $user->id) }}" class="btn btn-primary btn-sm">
                                        <span>Educations</span>
                                        <span class="badge badge-primary">{{ $user->educations_count }}</span>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.user.experience.index', $user->id) }}" class="btn btn-primary btn-sm">
                                        <span>Experiences</span>
                                        <span class="badge badge-primary">{{ $user->experiences_count }}</span>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.user.document.index', $user->id) }}" class="btn btn-primary btn-sm">
                                        <span>Documents</span>
                                        <span class="badge badge-primary">{{ $user->documents_count }}</span>
                                    </a>
                                </td>
                                <td>
                                    <a href="javascript:void(0)" data-id="{{ $user->id }}" class="btn btn-info btn-sm showUserDetail">
                                        <i class="fas fa-eye"></i> View
                                    </a>
                                    <a href="javascript:;;" class="btn btn-danger btn-sm" onclick="deleteUserModal({{ $user->id }})">
                                        <i class="fas fa-trash-alt"></i> Delete
                                    </a>
                                </td>
                            </tr>
                        @empty
                        <tr class="text-center">
                            <td colspan="6" class="text-muted">No data available</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- Card footer -->
            <div class="card-footer py-4">
                {{ $users->links() }}
            </div>
        </div>
    </div>
</div>

{{-- Modal Show User Detail --}}
<div class="modal fade" id="showUserDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-secondary">
            <div class="modal-header bg-default">
                <h1 class="modal-title text-secondary" id="exampleModalLabel">Detail User</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="text-secondary" aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mb-3">
                    <div class="col-12 text-center">
                        <img src="" alt="user profile image" id="image_profile" class="rounded" width="200" height="200">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-8">
                        <label for="full_name" class="form-control-label">Full Name</label>
                        <input type="text" id="full_name" class="form-control form-control-alternative" readonly>
                    </div>
                    <div class="col-4">
                        <label for="email" class="form-control-label">Email</label>
                        <input type="text" id="email" class="form-control form-control-alternative" readonly>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-6">
                        <label for="name" class="form-control-label">Name</label>
                        <input type="text" id="name" class="form-control form-control-alternative" readonly>
                    </div>
                    <div class="col-6">
                        <label for="phone_number" class="form-control-label">Phone Number</label>
                        <input type="text" id="phone_number" class="form-control form-control-alternative" readonly>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-12">
                        <label for="address" class="form-control-label">Address</label>
                        <textarea name="address" id="address" cols="30" rows="3" class="form-control form-control-alternative" readonly></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <label for="gender" class="form-control-label">Gender</label>
                        <input type="text" id="gender" class="form-control form-control-alternative" readonly>
                    </div>
                    <div class="col-4">
                        <label for="birth_date" class="form-control-label">Birth Date</label>
                        <input type="text" id="birth_date" class="form-control form-control-alternative" readonly>
                    </div>
                    <div class="col-2">
                        <label for="age" class="form-control-label">Age</label>
                        <input type="text" id="age" class="form-control form-control-alternative" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
{{-- End Modal Show User Detail --}}

{{-- Modal Delete User --}}
<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <form id="deleteUser" class="d-none" action="" method="post">
            @csrf
            @method('delete')

        </form>
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Delete User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Are you sure you want to delete this data?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-danger" onclick="$('#deleteUser').submit()">Yes</button>
        </div>
        </div>
    </div>
</div>
{{-- End Modal Delete User --}}
@endsection

@section('scripts')
<script>
    $(document).ready(function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".showUserDetail").click(function () {
            var id = $(this).data('id');
            var route = "user/detail/" + id;

            $.ajax({
                type: "GET",
                url: route,
                success: function (data) {
                    $('#full_name').val(data.full_name);
                    $('#name').val(data.name);
                    $('#email').val(data.email);
                    $('#birth_date').val(data.birth_date);
                    $('#age').val(data.user_age + ' Tahun');
                    $('#gender').val(data.gender);
                    $('#address').val(data.address);
                    $('#phone_number').val(data.phone_number);
                    if (data.image !== null) {
                        $('#image_profile').attr('src', "/image/user/profile/" + data.image);
                    }
                    $('#showUserDetailModal').modal('show');
                }
            });
        });
    })

    function deleteUserModal(id) {
        $('#deleteUser').attr('action', '{{url('admin/user/delete/')}}/'+id)
        $('#deleteUserModal').modal('show')
    }
</script>
@endsection
