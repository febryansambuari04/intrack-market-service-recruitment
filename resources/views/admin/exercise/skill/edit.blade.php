@extends('admin.layouts.default')

@section('header')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Exercise</li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.exercise.skill.index') }}">Skill</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Edit</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <!-- Card header -->
            <div class="card-header border-1" style="background-color: #f6f9fc;">
                <div class="row">
                    <div class="col-6">
                        <h3 class="mb-0">Edit Question</h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('admin.exercise.skill.update', $skill->id) }}" autocomplete="off">
                    @csrf

                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('division_id') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-division">{{ __('Division') }} <span class="text-danger">*</span></label>
                                    <select class="form-control form-control-alternative" id="input-division" name="division_id">
                                        <option value="">{{ __('--- Select Division ---') }}</option>
                                        @foreach ($divisions as $division)
                                            <option {{ (old('division_id') ?? $skill->division_id) == $division->id ? 'selected' : '' }} value="{{ $division->id }}">{{ $division->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group{{ $errors->has('question') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-question">{{ __('Question') }} <span class="text-danger">*</span></label>
                                    <textarea name="question" class="form-control form-control-alternative" id="input-question" rows="3" placeholder="{{ __('Question') }}">{{ old('question') ?? $skill->question }}</textarea>

                                    @if ($errors->has('question'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('question') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            @for ($i = 1; $i < 5; $i++)
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('answer_'.$i) ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-answer-{{ $i }}">{{ __('Answer ' . $i) }} <span class="text-danger">*</span></label>
                                        <textarea name="answer_{{ $i }}" class="form-control form-control-alternative" id="input-answer-{{ $i }}" rows="3" placeholder="{{ __('Answer ' . $i) }}">{{ old('answer_'.$i) ?? $skill->{'answer_'.$i} }}</textarea>

                                        @if ($errors->has('answer_'.$i))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('answer_'.$i) }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            @endfor
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('right_answer') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-right-answer">{{ __('Right Answer') }} <span class="text-danger">*</span></label>
                                    <textarea name="right_answer" class="form-control form-control-alternative" id="input-right-answer" rows="3" placeholder="{{ __('Right Answer') }}">{{ old('right_answer') ?? $skill->right_answer }}</textarea>

                                    @if ($errors->has('right_answer'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('right_answer') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label">{{ __('Status') }} <span class="text-danger">*</span></label>
                                    <select name="publish" class="form-control form-control-alternative">
                                        <option value="1" {{ $skill->publish ? 'selected' : '' }}>Publish</option>
                                        <option value="0" {{ ! $skill->publish ? 'selected' : '' }}>Draft</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <a href="{{ route('admin.exercise.psychological.index') }}" class="btn btn-secondary">{{ __('Back') }}</a>
                            <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
