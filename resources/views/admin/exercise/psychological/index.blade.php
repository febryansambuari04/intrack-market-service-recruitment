@extends('admin.layouts.default')

@section('header')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Exercise</li>
                            <li class="breadcrumb-item active" aria-current="page">Psychological</li>
                        </ol>
                    </nav>
                </div>
                @if (session('psychologicalTestMessage'))
                    <div class="col-lg-6 col-5">
                        <div class="alert alert-success alert-dismissible fade show p-2" role="alert">
                            <span class="alert-icon ml-3"><i class="ni ni-check-bold"></i></span> {{ session('psychologicalTestMessage') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
                <div class="row">
                    <div class="col-6">
                        <h3 class="mb-0">Psychological Test List</h3>
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{ route('admin.exercise.psychological.create') }}" class="btn btn-primary btn-sm">
                            <i class="fas fa-plus"></i> Add Question
                        </a>
                    </div>
                </div>
            </div>
            <!-- Light table -->
            <div class="table-responsive">
                <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">Question</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        @forelse ($psychological as $psychologic)
                            <tr>
                                <td>{{ $psychologic->question }}</td>
                                <td>
                                    @if ($psychologic->publish)
                                        <span class="badge badge-success">Publish</span>
                                    @else
                                        <span class="badge badge-warning">Draft</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="javascript:void(0)" id="showPsychologicalDetail" data-id="{{ $psychologic->id }}" class="btn btn-info btn-sm showPsychologicalDetail">
                                        <i class="fas fa-eye"></i> View
                                    </a>
                                    <a href="{{ route('admin.exercise.psychological.edit', $psychologic->id) }}" class="btn btn-warning btn-sm">
                                        <i class="fas fa-edit"></i> Edit
                                    </a>
                                    <a href="javascript:;;" class="btn btn-danger btn-sm" onclick="deletePsychologicalModal({{ $psychologic->id }})">
                                        <i class="fas fa-trash-alt"></i> Delete
                                    </a>
                                </td>
                            </tr>
                        @empty
                        <tr class="text-center">
                            <td colspan="3" class="text-muted">No data available</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>

                
            </div>
        </div>
    </div>
</div>

{{-- Modal Show Psychological Detail --}}
<div class="modal fade" id="showPsychologicalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-secondary">
            <div class="modal-header bg-default">
                <h1 class="modal-title text-secondary" id="exampleModalLabel">Detail Psychological Test</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="text-secondary" aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mb-3">
                    <div class="col-md-12">
                        <label for="question" class="form-control-label">Question</label>
                        <textarea name="question" id="question" cols="30" rows="3" class="form-control form-control-alternative" readonly></textarea>
                    </div>
                </div>

                <div class="row mb-3">
                    @for ($i = 1; $i < 5; $i++)
                        <div class="col-md-6">
                            <label for="answer_{{ $i }}" class="form-control-label">Answer {{ $i }}</label>
                            <textarea name="answer_{{ $i }}" id="answer_{{ $i }}" cols="30" rows="3" class="form-control form-control-alternative" readonly></textarea>
                        </div>
                    @endfor
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
{{-- End Modal Show Psychological Detail --}}

{{-- Modal Delete Psychological --}}
<div class="modal fade" id="deletePsychologicalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <form id="deletePsychological" class="d-none" action="" method="post">
            @csrf
            @method('delete')

        </form>
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Delete Psychological</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Are you sure you want to delete this data?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-danger" onclick="$('#deletePsychological').submit()">Yes</button>
        </div>
        </div>
    </div>
</div>
{{-- End Modal Delete Psychological --}}
@endsection

@section('scripts')
<script>
    $(document).ready(function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".showPsychologicalDetail").click(function () {
            var id = $(this).data('id');
            var route = "psychological/detail/" + id;

            console.log(route);

            $.ajax({
                type: "GET",
                url: route,
                success: function (data) {
                    $('#question').val(data.question);
                    $('#answer_1').val(data.answer_1);
                    $('#answer_2').val(data.answer_2);
                    $('#answer_3').val(data.answer_3);
                    $('#answer_4').val(data.answer_4);
                    $('#showPsychologicalModal').modal('show');
                }
            });
        });
    })

    function deletePsychologicalModal(id) {
        $('#deletePsychological').attr('action', '{{url('admin/exercise/psychological/delete/')}}/'+id)
        $('#deletePsychologicalModal').modal('show')
    }
</script>
@endsection
