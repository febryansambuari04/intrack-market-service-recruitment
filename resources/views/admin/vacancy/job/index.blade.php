@extends('admin.layouts.default')

@section('header')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Vacancy</li>
                            <li class="breadcrumb-item active" aria-current="page">Job</li>
                        </ol>
                    </nav>
                </div>
                @if (session('vacancyJobMessage'))
                    <div class="col-lg-6 col-5">
                        <div class="alert alert-success alert-dismissible fade show p-2" role="alert">
                            <span class="alert-icon ml-3"><i class="ni ni-check-bold"></i></span> {{ session('vacancyJobMessage') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
                <div class="row">
                    <div class="col-6">
                        <h3 class="mb-0">Job List</h3>
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{ route('admin.vacancy.job.create') }}" class="btn btn-primary btn-sm">
                            <i class="fas fa-plus"></i> Add Job
                        </a>
                    </div>
                </div>
            </div>
            <!-- Light table -->
            <div class="table-responsive">
                <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Level</th>
                            <th scope="col">Location</th>
                            <th scope="col">End Date</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        @forelse ($jobs as $job)
                            <tr>
                                <td>{{ $job->job_name }}</td>
                                <td>{{ $job->level_name }}</td>
                                <td>{{ $job->job_location }}</td>
                                <td>{{ $job->job_period_date }}</td>
                                <td>
                                    @if ($job->publish)
                                        <span class="badge badge-success">Publish</span>
                                    @else
                                        <span class="badge badge-warning">Draft</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="javascript:void(0)" id="showJobDetail" data-id="{{ $job->id }}" class="btn btn-info btn-sm showJobDetail">
                                        <i class="fas fa-eye"></i> View
                                    </a>
                                    <a href="{{ route('admin.vacancy.job.edit', $job->id) }}" class="btn btn-warning btn-sm">
                                        <i class="fas fa-edit"></i> Edit
                                    </a>
                                    <a href="javascript:;;" class="btn btn-danger btn-sm" onclick="deleteJobModal({{ $job->id }})">
                                        <i class="fas fa-trash-alt"></i> Delete
                                    </a>
                                </td>
                            </tr>
                        @empty
                        <tr class="text-center">
                            <td colspan="5" class="text-muted">No data available</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- Card footer -->
            <div class="card-footer py-4">
                {{ $jobs->links() }}
            </div>
        </div>
    </div>
</div>

{{-- Modal Show Job Detail --}}
<div class="modal fade" id="showJobDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-secondary">
            <div class="modal-header bg-default">
                <h1 class="modal-title text-secondary" id="exampleModalLabel">Detail Job</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="text-secondary" aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mb-3">
                    <div class="col-4">
                        <label for="job_name" class="form-control-label">Job Name</label>
                        <input type="text" id="job_name" class="form-control form-control-alternative" readonly>
                    </div>
                    <div class="col-4">
                        <label for="job_type" class="form-control-label">Job Type</label>
                        <input type="text" id="job_type" class="form-control form-control-alternative" readonly>
                    </div>
                    <div class="col-4">
                        <label for="division" class="form-control-label">Division</label>
                        <input type="text" id="division" class="form-control form-control-alternative" readonly>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-4">
                        <label for="job_level" class="form-control-label">Job Level</label>
                        <input type="text" id="job_level" class="form-control form-control-alternative" readonly>
                    </div>
                    <div class="col-4">
                        <label for="job_location" class="form-control-label">Job Location</label>
                        <input type="text" id="job_location" class="form-control form-control-alternative" readonly>
                    </div>
                    <div class="col-4">
                        <label for="job_period_date" class="form-control-label">Period Date</label>
                        <input type="text" id="job_period_date" class="form-control form-control-alternative" readonly>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-12">
                        <label for="job_short_description" class="form-control-label">Job Short Description</label>
                        <textarea name="job_short_description" id="job_short_description" cols="30" rows="3" class="form-control form-control-alternative" readonly></textarea>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-12">
                        <label for="job_description" class="form-control-label">Job Description</label>
                        <textarea name="job_description" id="job_description" cols="30" rows="3" class="form-control form-control-alternative" readonly></textarea>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-12">
                        <label for="job_requirement" class="form-control-label">Job Requirement</label>
                        <textarea name="job_requirement" id="job_requirement" cols="30" rows="3" class="form-control form-control-alternative" readonly></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
{{-- End Modal Show Job Detail --}}

{{-- Modal Delete Job --}}
<div class="modal fade" id="deleteJobModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <form id="deleteJob" class="d-none" action="" method="post">
            @csrf
            @method('delete')

        </form>
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Delete Job</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Are you sure you want to delete this data?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-danger" onclick="$('#deleteJob').submit()">Yes</button>
        </div>
        </div>
    </div>
</div>
{{-- End Modal Delete User --}}
@endsection

@section('scripts')
<script>
    $(document).ready(function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".showJobDetail").click(function () {
            var id = $(this).data('id');
            var route = "job/detail/" + id;

            console.log(route);

            $.ajax({
                type: "GET",
                url: route,
                success: function (data) {
                    $('#division').val(data.division_name);
                    $('#job_name').val(data.job_name);
                    $('#job_type').val(data.job_type_name);
                    $('#job_level').val(data.level_name);
                    $('#job_location').val(data.job_location);
                    $('#job_period_date').val(data.job_period_date);
                    $('#job_short_description').val(data.job_short_description);
                    $('#job_description').val(data.job_description);
                    $('#job_requirement').val(data.job_requirement);
                    $('#showJobDetailModal').modal('show');
                }
            });
        });
    })

    function deleteJobModal(id) {
        $('#deleteJob').attr('action', '{{url('admin/vacancy/job/delete/')}}/'+id)
        $('#deleteJobModal').modal('show')
    }
</script>
@endsection
