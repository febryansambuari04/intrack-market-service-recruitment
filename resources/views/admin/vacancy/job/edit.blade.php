@extends('admin.layouts.default')

@section('header')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Vacancy</li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.vacancy.job.index') }}">Job</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Edit</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <!-- Card header -->
            <div class="card-header border-1" style="background-color: #f6f9fc;">
                <div class="row">
                    <div class="col-6">
                        <h3 class="mb-0">Edit Job</h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('admin.vacancy.job.update', $job->id) }}" autocomplete="off">
                    @csrf

                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('job_name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-job-name">{{ __('Job Name') }} <span class="text-danger">*</span></label>
                                    <input type="text" name="job_name" id="input-job-name" class="form-control form-control-alternative{{ $errors->has('job_name') ? ' is-invalid' : '' }}" placeholder="{{ __('Job Name') }}" value="{{ old('job_name') ?? $job->job_name }}" autofocus required>

                                    @if ($errors->has('job_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('job_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('job_type') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-job-type">{{ __('Type') }} <span class="text-danger">*</span></label>
                                    <select class="form-control form-control-alternative{{ $errors->has('level_id') ? ' is-invalid' : '' }}" id="input-job-type" name="job_type">
                                        <option value="">{{ __('--- Select Type ---') }}</option>
                                        <option value="fulltime" {{ $job->job_type == 'fulltime' ? 'selected' : '' }}>Full Time</option>
                                        <option value="parttime" {{ $job->job_type == 'parttime' ? 'selected' : '' }}>Part Time</option>
                                        <option value="freelance" {{ $job->job_type == 'freelance' ? 'selected' : '' }}>Freelance</option>
                                        <option value="internship" {{ $job->job_type == 'internship' ? 'selected' : '' }}>Internship</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('division_id') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-division">{{ __('Division') }} <span class="text-danger">*</span></label>
                                    <select class="form-control form-control-alternative" id="input-division" name="division_id">
                                        <option value="">{{ __('--- Select Division ---') }}</option>
                                        @foreach ($divisions as $division)
                                            <option {{ (old('division_id') ?? $job->division_id) == $division->id ? 'selected' : '' }} value="{{ $division->id }}">{{ $division->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('level_id') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-job-level">{{ __('Level') }} <span class="text-danger">*</span></label>
                                    <select class="form-control form-control-alternative" id="input-job-level" name="level_id">
                                        <option value="">{{ __('--- Select Level ---') }}</option>
                                        @foreach ($levels as $level)
                                            <option {{ (old('level_id') ?? $job->level_id) == $level->id ? 'selected' : '' }} value="{{ $level->id }}">{{ $level->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('job_location') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-job-location">{{ __('Location') }} <span class="text-danger">*</span></label>
                                    <input type="text" name="job_location" id="input-job-location" class="form-control form-control-alternative{{ $errors->has('job_location') ? ' is-invalid' : '' }}" placeholder="{{ __('Job Location') }}" value="{{ old('job_location') ?? $job->job_location }}" required>

                                    @if ($errors->has('job_location'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('job_location') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('job_period_date') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-job-period-date">{{ __('Period Date') }} <span class="text-danger">*</span></label>
                                    <input type="date" name="job_period_date" id="input-job-period-date" class="form-control form-control-alternative{{ $errors->has('job_period_date') ? ' is-invalid' : '' }}" placeholder="{{ __('Job Period Date') }}" value="{{ old('job_period_date') ?? $job->job_period_date }}" required>

                                    @if ($errors->has('job_period_date'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('job_period_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group{{ $errors->has('job_short_description') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-job-short-description">{{ __('Short Description') }} <span class="text-danger">*</span></label>
                                    <textarea name="job_short_description" class="form-control form-control-alternative" id="input-job-short-description" rows="3" placeholder="{{ __('Job Short Description') }}">{{ old('job_short_description') ?? $job->job_short_description }}</textarea>

                                    @if ($errors->has('job_short_description'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('job_short_description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group{{ $errors->has('job_description') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-job-description">{{ __('Job Description') }} <span class="text-danger">*</span></label>
                                    <textarea name="job_description" class="form-control form-control-alternative ckeditor" id="input-job-description" rows="3">{{ old('job_description') ?? $job->job_description }}</textarea>

                                    @if ($errors->has('job_description'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('job_description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group{{ $errors->has('job_requirement') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-job-requirement">{{ __('Job Requirement') }} <span class="text-danger">*</span></label>
                                    <textarea name="job_requirement" class="form-control form-control-alternative ckeditor" id="input-job-requirement" rows="3">{{ old('job_requirement') ?? $job->job_requirement }}</textarea>

                                    @if ($errors->has('job_requirement'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('job_requirement') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label">{{ __('Status') }} <span class="text-danger">*</span></label>
                                    <select name="publish" class="form-control form-control-alternative">
                                        <option value="1" {{ $job->publish ? 'selected' : '' }}>Publish</option>
                                        <option value="0" {{ ! $job->publish ? 'selected' : '' }}>Draft</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <a href="{{ route('admin.vacancy.job.index') }}" class="btn btn-secondary">{{ __('Back') }}</a>
                            <input type="hidden" name="id" value="{{ $division->id }}">
                            <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
