@extends('admin.layouts.default')

@section('header')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Vacancy</li>
                            <li class="breadcrumb-item active" aria-current="page">Level</li>
                        </ol>
                    </nav>
                </div>
                @if (session('vacancyLevelMessage'))
                    <div class="col-lg-6 col-5">
                        <div class="alert alert-success alert-dismissible fade show p-2" role="alert">
                            <span class="alert-icon ml-3"><i class="ni ni-check-bold"></i></span> {{ session('vacancyLevelMessage') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
                <div class="row">
                    <div class="col-6">
                        <h3 class="mb-0">Level List</h3>
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{ route('admin.vacancy.level.create') }}" class="btn btn-primary btn-sm">
                            <i class="fas fa-plus"></i> Add Level
                        </a>
                    </div>
                </div>
            </div>
            <!-- Light table -->
            <div class="table-responsive">
                <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        @forelse ($levels as $level)
                            <tr>
                                <td>{{ $level->name }}</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>
                                    <a href="{{ route('admin.vacancy.level.edit', $level->id) }}" class="btn btn-warning btn-sm">
                                        <i class="fas fa-edit"></i> Edit
                                    </a>
                                    <a href="javascript:;;" class="btn btn-danger btn-sm" onclick="deleteLevelModal({{ $level->id }})">
                                        <i class="fas fa-trash-alt"></i> Delete
                                    </a>
                                </td>
                            </tr>
                        @empty
                        <tr class="text-center">
                            <td colspan="4" class="text-muted">No data available</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

{{-- Modal Delete Level --}}
<div class="modal fade" id="deleteLevelModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <form id="deleteLevel" class="d-none" action="" method="post">
            @csrf
            @method('delete')

        </form>
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Delete Level</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Are you sure you want to delete this data?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-danger" onclick="$('#deleteLevel').submit()">Yes</button>
        </div>
        </div>
    </div>
</div>
{{-- End Modal Delete Level --}}
@endsection

@section('scripts')
<script>
    function deleteLevelModal(id) {
        $('#deleteLevel').attr('action', '{{url('admin/vacancy/level/delete/')}}/'+id)
        $('#deleteLevelModal').modal('show')
    }
</script>
@endsection
