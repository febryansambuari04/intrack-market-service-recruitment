@extends('admin.layouts.default')

@section('header')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Vacancy</li>
                            <li class="breadcrumb-item active" aria-current="page">Division</li>
                        </ol>
                    </nav>
                </div>
                @if (session('vacancyDivisionMessage'))
                    <div class="col-lg-6 col-5">
                        <div class="alert alert-success alert-dismissible fade show p-2" role="alert">
                            <span class="alert-icon ml-3"><i class="ni ni-check-bold"></i></span> {{ session('vacancyDivisionMessage') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
                <div class="row">
                    <div class="col-6">
                        <h3 class="mb-0">Division List</h3>
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{ route('admin.vacancy.division.create') }}" class="btn btn-primary btn-sm">
                            <i class="fas fa-plus"></i> Add Division
                        </a>
                    </div>
                </div>
            </div>
            <!-- Light table -->
            <div class="table-responsive">
                <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Description</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        @forelse ($divisions as $division)
                            <tr>
                                <td>
                                    <div class="media align-items-center">
                                        <a href="#" class="avatar mr-3">
                                            <img alt="Division Icon" src="/image/admin/vacancy/division/icon/{{ $division->icon }}">
                                        </a>
                                        <div class="media-body">
                                            <span class="name mb-0 text-sm">{{ $division->name }}</span>
                                        </div>
                                    </div>
                                </td>
                                <td>{{ $division->description }}</td>
                                <td>
                                    <a href="{{ route('admin.vacancy.division.edit', $division->id) }}" class="btn btn-warning btn-sm">
                                        <i class="fas fa-edit"></i> Edit
                                    </a>
                                    <a href="javascript:;;" class="btn btn-danger btn-sm" onclick="deleteDivisionModal({{ $division->id }})">
                                        <i class="fas fa-trash-alt"></i> Delete
                                    </a>
                                </td>
                            </tr>
                        @empty
                        <tr class="text-center">
                            <td colspan="3" class="text-muted">No data available</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- Card footer -->
            <div class="card-footer py-4">
                <div class="row justify-content-center">
                    {{ $divisions->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal Delete Division --}}
<div class="modal fade" id="deleteDivisionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <form id="deleteDivision" class="d-none" action="" method="post">
            @csrf
            @method('delete')

        </form>
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Delete User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Are you sure you want to delete this data?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button type="button" class="btn btn-danger" onclick="$('#deleteDivision').submit()">Yes</button>
        </div>
        </div>
    </div>
</div>
{{-- End Modal Delete Division --}}
@endsection

@section('scripts')
<script>
    function deleteDivisionModal(id) {
        $('#deleteDivision').attr('action', '{{url('admin/vacancy/division/delete/')}}/'+id)
        $('#deleteDivisionModal').modal('show')
    }
</script>
@endsection
