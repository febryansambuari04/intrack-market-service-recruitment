<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="javascript:void(0)">
                <h3>Intrack Market Services</h3>
            </a>
        </div>
        <div class="navbar-inner mt--3">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">

                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link @if(Route::currentRouteName() === 'admin.dashboard') active @endif" href="{{ route('admin.dashboard') }}">
                            <i class="ni ni-planet text-primary"></i>
                            <span class="nav-link-text">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#applicant" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="vacancy">
                            <i class="ni ni-briefcase-24 text-primary"></i>
                            <span class="nav-link-text">Applicant</span>
                        </a>

                        <div class="collapse" id="applicant">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::is('admin/user*') ? 'active' : '' }}" href="{{ route('admin.user.index') }}">
                                        <i class="ni ni-circle-08 text-primary"></i>
                                        <span class="nav-link-text">Users</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::is('admin/user/status*') ? 'active' : '' }}" href="{{ route('admin.user.status.index') }}">
                                        <i class="ni ni-air-baloon text-primary"></i>
                                        <span class="nav-link-text">Status</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#vacancy" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="vacancy">
                            <i class="ni ni-briefcase-24 text-primary"></i>
                            <span class="nav-link-text">Vacancy</span>
                        </a>

                        <div class="collapse" id="vacancy">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::is('admin/vacancy/division*') ? 'active' : '' }}" href="{{ route('admin.vacancy.division.index') }}">
                                        <i class="ni ni-archive-2 text-primary"></i>
                                        <span class="nav-link-text">Divisions</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::is('admin/vacancy/level*') ? 'active' : '' }}" href="{{ route('admin.vacancy.level.index') }}">
                                        <i class="ni ni-badge text-primary"></i>
                                        <span class="nav-link-text">Levels</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::is('admin/vacancy/job*') ? 'active' : '' }}" href="{{ route('admin.vacancy.job.index') }}">
                                        <i class="ni ni-briefcase-24 text-primary"></i>
                                        <span class="nav-link-text">Jobs</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#exercise" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="exercise">
                            <i class="ni ni-bullet-list-67 text-primary"></i>
                            <span class="nav-link-text">Exercise</span>
                        </a>

                        <div class="collapse" id="exercise">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::is('admin/exercise/psychological*') ? 'active' : '' }}" href="{{ route('admin.exercise.psychological.index') }}">
                                        <i class="ni ni-book-bookmark text-primary"></i>
                                        <span class="nav-link-text">Psychological Test</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::is('admin/exercise/skill*') ? 'active' : '' }}" href="{{ route('admin.exercise.skill.index') }}">
                                        <i class="ni ni-ruler-pencil text-primary"></i>
                                        <span class="nav-link-text">Skill Test</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    @if(in_array(Auth::guard('admin')->user()->role, ['superadmin'] ))
                        <li class="nav-item">
                            <a class="nav-link @if(Route::currentRouteName() === 'admin.management*') active @endif" href="{{ route('admin.management.index') }}">
                                <i class="ni ni-single-02 text-primary"></i>
                                <span class="nav-link-text">Admin Management</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</nav>
