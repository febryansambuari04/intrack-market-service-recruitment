<div class="container">
    <footer class="footer">
        <div class="row align-items-end justify-content-lg-between">
            <div class="col-lg-6">
                <div class="copyright text-center text-lg-left text-muted">
                    &copy; {{ now()->year }} <span class="font-weight-bold ml-1">Intracks Market Services</span>
                </div>
            </div>
            <div class="col-lg-6 text-right">
                <small class="text-muted">
                    Made with <i class="fas fa-heart text-danger"></i>
                </small>
            </div>
        </div>
    </footer>
</div>
