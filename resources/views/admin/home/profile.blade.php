@extends('admin.layouts.default')

@section('header')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">My Profile</li>
                        </ol>
                    </nav>
                </div>
                @if (session('updateAdminProfileMessage'))
                    <div class="col-lg-6 col-5">
                        <div class="alert alert-success alert-dismissible fade show p-2" role="alert">
                            <span class="alert-icon ml-3"><i class="ni ni-check-bold"></i></span> {{ session('updateAdminProfileMessage') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif

                @if (session('current_password'))
                    <div class="col-lg-6 col-5">
                        <div class="alert alert-danger alert-dismissible fade show p-2" role="alert">
                            <span class="alert-icon ml-3"><i class="ni ni-fat-remove"></i></span> {{ session('current_password') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection

@section('content')
<div class="col-xl-12 order-xl-1">
    <div class="card">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-0">Edit profile </h3>
                </div>
            </div>
        </div>
        <div class="card-body">
            <h6 class="heading-small text-muted mb-4">User information</h6>
            <div class="pl-lg-4">
                <form action="{{ route('admin.profile.update') }}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label" for="input-username">Name</label>
                                <input type="text" name="name" id="input-username" class="form-control form-control-alternative" placeholder="Username"
                                    value="{{ old('name') ?? $admin->name }}">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label" for="input-email">Email address</label>
                                <input type="email" name="email" id="input-email" class="form-control form-control-alternative"
                                    placeholder="Email" value="{{ old('email') ?? $admin->email }}">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label" for="input-role">Role</label>
                                <input type="text" name="role" id="input-role" class="form-control form-control-alternative"
                                    placeholder="Role" value="{{ old('role') ?? $admin->role }}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary btn-sm">Update Profile</button>
                        </div>
                    </div>
                </form>
            </div>
            <hr class="my-4" />
            <!-- Password -->
            <h6 class="heading-small text-muted mb-4">Password</h6>
            <div class="pl-lg-4">
                <form action="{{ route('admin.profile.password.update') }}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label" for="input-current-password">Current Password</label>
                                <input type="password" name="current_password" id="input-current-password" class="form-control form-control-alternative @error('current_password') is-invalid @enderror" placeholder="Current Password">
                                @error('current_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label" for="input-new-password">New Password</label>
                                <input type="password" name="new_password" id="input-new-password" class="form-control form-control-alternative @error('new_password') is-invalid @enderror" placeholder="New Password">
                                @error('new_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label" for="input-confirm-new-password">Confirm New Password</label>
                                <input type="password" name="confirm_new_password" id="input-confirm-new-password" class="form-control form-control-alternative @error('confirm_new_password') is-invalid @enderror" placeholder="Confirm New Password">
                                @error('confirm_new_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary btn-sm">Change Password</button>
                        </div>
                    </div>
                </form>
            </div>
            <hr class="my-4" />
        </div>
    </div>
</div>
@endsection
