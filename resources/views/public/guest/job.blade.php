@extends('public.guest.layouts.default', ['class' => 'bg-default'])

@section('content')
    <div class="bg-gradient-success py-7 py-lg-8">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm">
                    <h1 class="display-1 mt-7 text-secondary">Million of jobs, finds the one thats rights for you</h1>
                    <a href="#division" class="btn btn-danger btn-lg mt-5">See Available Position</a>
                </div>
                <div class="col-md-7 col-sm my-auto">
                    <img src="{{ asset('assets/img/illustrations/choose position.svg') }}" alt="image_passion" width="700" />
                </div>
            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>

    <div class="bg-gradient-secondary py-7 py-lg-8" id="jobList">
        <div class="container">
            <div class="row mt--5">
                <div class="col-md-4 text-center">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-7">All Category</span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach ($divisions as $division)
                                <a class="dropdown-item" href="{{ route('job.filter', $division->id) }}#division">{{ $division->name }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <form class="navbar-search" method="get" action="{{ route('job.search') }}" id="search_jobs">
                        <div class="form-group mb-0">
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input class="form-control" placeholder="Type Your Keywords" type="text" name="q" value="{{ $q ?? '' }}" id="keyword_jobs">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            {{-- Job List --}}
            @foreach ($divisions as $division)
                @if (count($division->jobs) > 0)
                    <div id="division" class="division-card-{{ $division->id }}">
                        <div class="row mt-8 mb-5 justify-content-end" @if(Request::is('job')) hidden @endif>
                            <div class="col-md-2">
                                <a href="{{ route('job') }}" class="badge badge-pill badge-warning"><i class="ni ni-fat-remove"></i> Clear Filter</a>
                            </div>
                        </div>

                        <div class="row mt-5">
                            <div class="col-md-2 text-center">
                                <img src="{{ $division->division_icon_link }}" width="100" alt="Card image category finance">
                            </div>
                            <div class="col-md-10">
                                <h2>{{ ucwords($division->name) }}</h2>
                                <p class="text-muted">{{ $division->description }}</p>
                            </div>
                        </div>

                        <div class="row mt-4">
                            @foreach ($division->jobs as $job)
                                <div class="col-md-4">
                                    <div class="card job-list-card">
                                        <div class="card-body">
                                            <h5 class="card-title">{{ ucwords($job->job_name) }}</h5>
                                            <small class="card-text">{{ $job->job_short_description }}</small>
                                            <a href="javascript:void(0)" id="showJobDetail" data-id="{{ $job->id }}" class="btn btn-success btn-block mt-3 showJobDetail">See Details</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <hr class="my-5">
                    </div>
                @endif
            @endforeach
            {{-- End Job List --}}

            <div class="row bg-gradient-success mt-8 rounded py-6 px-5">
                <div class="col-md-9">
                    <h2 class="text-white">Already have a position that yout want ? Login now!</h2>
                </div>
                <div class="col-md-3 text-center">
                    <a href="{{ route('login') }}" class="btn btn-danger btn-lg rounded btn-block">
                        <span class="btn-inner--icon mr-3">
                            <i class="ni ni-key-25"></i>
                        </span>
                        <span class="btn-inner--text">Login</span>
                    </a>
                </div>
            </div>

            {{-- Apply Job Modal --}}
            <div class="modal fade" tabindex="-1" id="showAlertJobApply" role="dialog" aria-labelledby="apply" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content bg-gradient-danger">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="py-3 text-center">
                                <i class="ni ni-bell-55 ni-3x text-white"></i>
                                <h4 class="heading text-white mt-4" id="alertJobApply">You should read this!</h4>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="{{ route('login') }}" class="btn btn-white">Login</a>
                            <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- End Apply Job Modal --}}

            {{-- Detail Modal --}}
            <div class="modal fade" id="showJobDetailModal" tabindex="-1" role="dialog" aria-labelledby="job_name" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title" id="job_name"></h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row border">
                                <div class="col-sm border">
                                    <h4 id="division_name"></h4>
                                </div>
                                <div class="col-sm border">
                                    <h4 id="job_level"></h4>
                                </div>
                                <div class="col-sm border">
                                    <h4 id="job_location"></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm text-right">
                                    <small class="text-muted" id="job_period_date"></small>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col-sm-12">
                                    <h4>Description</h4>
                                    <p class="text-muted" id="job_description"></p>
                                </div>
                                <div class="col-sm-12 mt-3">
                                    <h4>Job Requirement</h4>
                                    <p class="text-muted" id="job_requirement"></p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success apply">Apply</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Detail Modal --}}
        </div>
    </div>

    <div class="container mt--10 pb-5"></div>
@endsection

@section('scripts')
<script>
    $(document).ready(function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".showJobDetail").click(function () {
            var id = $(this).data('id');
            var route = "job/detail/" + id;

            $.ajax({
                type: "GET",
                url: route,
                success: function (data) {
                    console.log(data);
                    $('#division_name').text("Division : " + data.division_name);
                    $('#job_name').text(data.job_name);
                    $('#job_level').text("Level : " + data.level_name);
                    $('#job_location').text("Location : " + data.job_location);
                    $('#job_period_date').text("Applications are open until " + data.job_period_date);
                    $('#job_short_description').val(data.job_short_description);
                    $('#job_description').html(data.job_description);
                    $('#job_requirement').html(data.job_requirement);
                    $('#showJobDetailModal').modal('show');
                }
            });
        });

        $(".apply").click(function () {
            var route = "job/apply";
            console.log(route);

            $.ajax({
                type: "GET",
                url: route,
                success: function (data) {
                    console.log(data);
                    $('#alertJobApply').text(data);
                    $('#showJobDetailModal').modal('hide');
                    $('#showAlertJobApply').modal('show');
                }
            });
        });

        $('#keyword_jobs').keypress(function (e) {
            var key = e.which;
            if (key == 13) {
                $('#search_jobs').submit();
            }
        })
    })
</script>
@endsection
