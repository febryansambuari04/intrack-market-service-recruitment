@extends('public.guest.layouts.default', ['class' => 'bg-default'])

@section('content')
<section class="header bg-gradient-primary py-7 py-lg-8">
    <div class="container">
        <div class="header-body text-center mt-7 mb-7">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-6">
                    <h1 class="text-white">Find your job better and faster with Intrack Market Services</h1>
                    <a href="#category" class="btn btn-danger btn-lg mt-5">Browse Position</a>
                </div>
            </div>
        </div>
    </div>
    <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
    </div>
</section>

<section class="bg-gradient-secondary py-7 py-lg-8">
    <div class="container">
        <div class="row">
            <div class="col-sm">
                <img src="{{ asset('assets/img/illustrations/passion.png') }}" width="450" alt="image_passion" />
            </div>
            <div class="col-sm">
                <h1 class="display-1 mt-7">Find your passion and achieve success</h1>
                <p class="text-muted mt-5">
                    find a job that suits your interests and talents. A high salary is not the top priority. Most importantly,You can work according to your heart's desire.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="bg-gradient-success py-7 py-lg-8">
    <div class="container">
        <div class="row">
            <div class="col-sm">
                <h1 class="display-1 mt-7 text-secondary">Million of jobs, finds the one thats rights for you</h1>
                <p class="text-secondary mt-5">
                    Get your blood tests delivered at home collect a sample from the news your blood tests.
                </p>
            </div>
            <div class="col-sm ml-5">
                <img src="{{ asset('assets/img/illustrations/jobs.png') }}" width="450" alt="image_passion" />
            </div>
        </div>
    </div>
    <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
    </div>
</section>

<section class="bg-gradient-secondary py-7 py-lg-8" id="category">
    <div class="container">
        <div class="row">
            <div class="col-md text-center">
                <h1 class="display-1">Browse jobs by Division</h1>
                <p class="mb-5">Choose from the list of most popular divisions</p>
            </div>
        </div>
        <div class="row">
            @foreach ($divisions as $division)
                <div class="col-sm-6 col-md-4 mb-4">
                    <div class="card custom-card text-center shadow p-5">
                        <img class="card-img-top" src="{{ $division->division_icon_link }}" alt="Card image category finance">
                        <div class="card-body">
                            <h3>{{ ucwords($division->name) }}</h3>
                            <p>{{ $division->jobs_count }} open position</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>

<section class="bg-gradient-success py-7 py-lg-8">
    <div class="container">
        <div class="row justify-content-center">
            <img src="{{ asset('assets/img/illustrations/jobs.png') }}" alt="image join with us" />
            <div class="col-md-12 text-center">
                <h1 class="display-1 text-secondary mt-5">Come on, join with us !</h1>
                <p class="text-secondary mt-3">
                    Create an account and refer your friend
                </p>
                <a href="{{ route('register') }}" class="btn btn-danger btn-lg mt-3">Create Account</a>
            </div>
        </div>
    </div>
    <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
    </div>
</section>

<div class="container mt--10 pb-5"></div>
@endsection

@section('scripts')
<script>
    $('.custom-card').each(function() {
        var link = $(this).html();
        $(this).contents().wrap('<a href="{{ route('job') }}"></a>');
    });
</script>
@endsection
