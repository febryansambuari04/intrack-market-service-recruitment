<footer class="py-5">
    <div class="container">
        <div class="row text-center">
            <div class="col-xl-12">
                <div class="copyright text-muted">
                    &copy; {{ now()->year }} PT. Sukses Bersama
                </div>
            </div>
        </div>
    </div>
</footer>
