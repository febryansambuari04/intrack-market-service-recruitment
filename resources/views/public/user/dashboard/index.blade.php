@extends('public.user.layouts.default')

@section('content')
<div class="container-fluid mt--7">
    @if ($userApplicationAccepted)
        <div class="row mb-5">
            <div class="col-md-12">
                <div class="card bg-gradient-default">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Congratulations!</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title text-white">You have been invited for Interview User. Please check information detail below.</h3>
                        <blockquote class="blockquote text-white mb-0">
                            <p>
                                {{ $userApplicationAccepted->description }}
                            </p>
                            <a href="{{ $userApplicationAccepted->zoom_url }}" target="_blank">Link Zoom</a>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if($userApplicationWaiting)
        <div class="row mb-5">
            <div class="col-md-12">
                <div class="alert alert-darker alert-dismissible fade show" role="alert">
                    <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
                    <span class="alert-inner--text"><strong>Congrats!</strong> You have apply a job!</span> <br>
                    <span class="alert-inner--text">We have to see your profile, if you qualified based on our requirement, We will notify you soon.</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    @endif

    @if ($userApplicationApplied)
        @if ($userApplicationApplied->isHavePsychologicalTest && $userApplicationApplied->isHaveSkillTest)
            <div class="row mb-5">
                <div class="col-md-12">
                    <div class="alert alert-default alert-dismissible fade show" role="alert">
                        <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
                        <span class="alert-inner--text"><strong>Congrats!</strong> You have finished all the test!</span> <br>
                        <span class="alert-inner--text">We will notify you for your result within 2 weeks. Please check back around that time.</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @else
            <div class="row mb-5">
                <div class="col-md-12">
                    <div class="alert alert-default alert-dismissible fade show" role="alert">
                        <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
                        <span class="alert-inner--text"><strong>Congrats!</strong> You have qualified our requirement!</span> <br>
                        <span class="alert-inner--text">Please take the online test below for the next step.</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif

        <div class="row mb-5">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Application Online Test</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Test</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Psychological</td>
                                    <td>
                                        @if ($userApplicationApplied->isHavePsychologicalTest)
                                            <a href="#" class="btn btn-outline-secondary btn-sm" disabled>Test Have Finished</a>
                                        @else
                                            <a href="{{ route('user.exam.psychological', ['applicant_id' => $userApplicationApplied->id]) }}" class="btn btn-sm btn-info">Take a Test</a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Skill</td>
                                    <td>
                                        @if ($userApplicationApplied->isHaveSkillTest)
                                            <a href="#" class="btn btn-outline-secondary btn-sm" disabled>Test Have Finished</a>
                                        @else
                                            <a href="{{ route('user.exam.skill', ['applicant_id' => $userApplicationApplied->id]) }}" class="btn btn-sm btn-info">Take a Test</a>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">

                        </nav>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-6">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Active Transactions</h3>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Position</th>
                                <th scope="col">Status</th>
                                <th scope="col">Applied Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($userApplications as $userApplication)
                                @if ($userApplication->status !== 'rejected')
                                    <tr>
                                        <td>{{ $userApplication->job->job_name }}</td>
                                        <td>
                                            @if ($userApplication->status === 'applied' || $userApplication->status === 'waiting')
                                                <span class="badge badge-pill badge-warning">Process</span>
                                            @elseif ($userApplication->status === 'rejected')
                                                <span class="badge badge-pill badge-danger">Rejected</span>
                                            @endif
                                        </td>
                                        <td>{{ $userApplication->created_at }}</td>
                                    </tr>
                                @endif
                            @empty
                                <tr>
                                    <td colspan="3" class="text-muted text-center">No Data</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">

                    </nav>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Inactive</h3>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Position</th>
                                <th scope="col">Status</th>
                                <th scope="col">Updated Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($userApplications as $userApplication)
                                @if ($userApplication->status === 'rejected')
                                <tr>
                                    <td>{{ $userApplication->job->job_name }}</td>
                                    <td>
                                        @if ($userApplication->status === 'applied')
                                            <span class="badge badge-pill badge-warning">Process</span>
                                        @elseif ($userApplication->status === 'rejected')
                                            <span class="badge badge-pill badge-danger">Rejected</span>
                                        @endif
                                    </td>
                                    <td>{{ $userApplication->updated_at }}</td>
                                </tr>
                                @endif
                            @empty
                                <tr>
                                    <td colspan="3" class="text-muted text-center">No Data</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">

                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h4 class="mb-0">Job recommendations</h4>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        @foreach ($jobs as $job)
                            <div class="col-md-4">
                                <div class="card job-list-card">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ ucwords($job->job_name) }}</h5>
                                        <small class="card-text">{{ $job->job_short_description }}</small>
                                        <a href="javascript:void(0)" id="showJobDetail" data-id="{{ $job->id }}" class="btn btn-success btn-block mt-3 showJobDetail">See Details</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                        <a href="{{ route('user.job') }}" class="text-primary text-success border-bottom">See All Available Position</a>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    {{-- Detail Modal --}}
    <div class="modal fade" id="showJobDetailModal" tabindex="-1" role="dialog" aria-labelledby="job_name" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title" id="job_name"></h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row border">
                        <div class="col-sm border">
                            <h4 id="division_name"></h4>
                        </div>
                        <div class="col-sm border">
                            <h4 id="job_level"></h4>
                        </div>
                        <div class="col-sm border">
                            <h4 id="job_location"></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm text-right">
                            <small class="text-muted" id="job_period_date"></small>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-sm-12">
                            <h4>Description</h4>
                            <p class="text-muted" id="job_description"></p>
                        </div>
                        <div class="col-sm-12 mt-3">
                            <h4>Job Requirement</h4>
                            <p class="text-muted" id="job_requirement"></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <a id="applyJobUrl" class="btn btn-success">Apply</a>
                </div>
            </div>
        </div>
    </div>
    {{-- Detail Modal --}}
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".showJobDetail").click(function () {
            var id = $(this).data('id');
            var route = "job/detail/" + id;

            $.ajax({
                type: "GET",
                url: route,
                success: function (data) {
                    $('#applyJobUrl').attr("href", "job/apply/" + data.id);
                    $('#division_name').text("Division : " + data.division_name);
                    $('#job_name').text(data.job_name);
                    $('#job_level').text("Level : " + data.level_name);
                    $('#job_location').text("Location : " + data.job_location);
                    $('#job_period_date').text("Applications are open until " + data.job_period_date);
                    $('#job_short_description').val(data.job_short_description);
                    $('#job_description').html(data.job_description);
                    $('#job_requirement').html(data.job_requirement);
                    $('#showJobDetailModal').modal('show');
                }
            });
        });
    })
</script>
@endsection
