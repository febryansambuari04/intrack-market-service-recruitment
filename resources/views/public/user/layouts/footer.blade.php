<footer class="py-5">
    <div class="container">
        <div class="row text-center">
            <div class="col-xl-12">
                <div class="copyright text-muted">
                    &copy; {{ now()->year }} Intrack Market Services
                </div>
            </div>
        </div>
    </div>
</footer>
