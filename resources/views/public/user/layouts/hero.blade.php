{{-- Jumbotron --}}
<div class="header bg-gradient-success pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row">
                                <div class="col-8">
                                    <h2 class="display-2 mb-0">Welcome, {{ auth()->user()->name }}</h2>
                                    <span class="text-muted">{{ Carbon\Carbon::now()->toDayDateTimeString(); }}</span>
                                    {{-- <p class="mt-5">
                                        <span class="badge badge-info py-2">
                                            <i class="ni ni-bulb-61"></i> Status : Tes Psikotes
                                        </span> <br>
                                        <small class="text-danger">
                                            * Psikotes harus dilakukan maksimal 72 jam dari tanggal apply
                                            lamaran pekerjaan
                                        </small>
                                    </p> --}}
                                </div>
                                <div class="col-4 text-right">
                                    @if (auth()->user()->image)
                                        <img alt="Image Profile" src="/image/user/profile/{{ auth()->user()->image }}" class="rounded-circle mr-5 mt-3" width="150">
                                    @else
                                    <img alt="Image placeholder" src="{{ asset('argon') }}/img/theme/team-4-800x800.jpg" class="rounded-circle mr-5 mt-3" width="150">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Jumbotron --}}
