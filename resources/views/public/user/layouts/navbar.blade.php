<!-- Top navbar -->
<nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
    <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase" href="{{ route('user.dashboard') }}">Intrack Market Services</a>

        <ul class="navbar-nav ml-lg-auto">
            <li class="nav-item">
                <a class="nav-link @if(Route::currentRouteName() === 'user.dashboard') active-top-navbar @endif" href="{{ route('user.dashboard') }}">
                    <span class="nav-link-inner--text">Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(Route::currentRouteName() === 'user.job') active-top-navbar @endif" href="{{ route('user.job') }}">
                    <span class="nav-link-inner--text">Job List</span>
                </a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link @if(Route::currentRouteName() === 'user.exam') active-top-navbar @endif" href="{{ route('user.exam') }}">
                    <span class="nav-link-inner--text">Online Test</span>
                </a>
            </li> --}}
        </ul>

        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
            <li class="nav-item dropdown">
                <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            @if (auth()->user()->image)
                                <img src="/image/user/profile/{{ auth()->user()->image }}" alt="Profile Image">
                            @else
                                <img alt="Image placeholder" src="{{ asset('argon') }}/img/theme/team-4-800x800.jpg">
                            @endif
                        </span>
                    </div>
                </a>

                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <a href="{{ route('user.profile', ['id' => auth()->user()->id]) }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>My profile</span>
                    </a>

                    <div class="dropdown-divider"></div>

                    <form action="/logout" method="post">
                        @csrf

                        <button class="dropdown-item" style="cursor: pointer">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </button>
                    </form>
                </div>
            </li>
        </ul>
    </div>
</nav>
<!-- Top navbar -->
