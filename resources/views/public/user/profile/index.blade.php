@extends('public.user.layouts.default')

@section('content')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-image: url(../../assets/img/illustrations/bg-profile.svg); background-size: cover; background-position: center;">
    <!-- Mask -->
    <span class="mask bg-gradient-default opacity-8"></span>
    <!-- Header container -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-7 ml-3">
                <h1 class="display-2 text-white">Hello <br> {{ $user->full_name ?? $user->name }}</h1>
                <p class="text-muted mt-0 mb-5">This is your profile page. You can change your data information before apply job</p>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
            <div class="card card-profile shadow">
                <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                        <div class="card-profile-image">
                            <a href="#">
                                @if ($user->image)
                                    <img src="/image/user/profile/{{ $user->image }}" alt="Profile Image" class="rounded">
                                @else
                                    <img src="{{ asset('argon') }}/img/theme/team-4-800x800.jpg" class="rounded-circle">
                                @endif
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body pt-0 pt-md-4 mt-8">
                    <div class="text-center">
                        <h3>
                            {{ $user->name }} <span class="font-weight-light">, 27</span>
                        </h3>
                        @if (count($user->educations) > 0)
                            <div>
                                <i class="ni education_hat mr-2"></i>{{ $user->educations[0] ? $user->educations[0]->school_name : '' }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        @if (session('success'))
            <div class="col-md-8">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <span class="alert-icon"><i class="ni ni-check-bold"></i></span> {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif

        <div class="col-xl-8 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="nav-wrapper p-3">
                    <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0 active" id="basic-info-tab" data-toggle="tab" href="#basic-info" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true">
                                <i class="ni ni-badge mr-2"></i> Basic Info
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0" id="education-tab" data-toggle="tab" href="#education" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false">
                                <i class="ni ni-ruler-pencil mr-2"></i> Educations
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0" id="experience-tab" data-toggle="tab" href="#experience" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false">
                                <i class="ni ni-briefcase-24 mr-2"></i> Experience
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0" id="document-tab" data-toggle="tab" href="#document" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false">
                                <i class="ni ni-paper-diploma mr-2"></i> Documents
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow">
                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            {{-- Basic Info Tab --}}
                            <div class="tab-pane fade show active" id="basic-info" role="tabpanel" aria-labelledby="basic-info-tab">
                                <form method="post" action="{{ route('user.profile.basic-info.update', auth()->user()->id) }}" autocomplete="off" enctype="multipart/form-data">
                                    @csrf

                                    <h6 class="heading-small text-muted mb-4">{{ __('User information') }}</h6>

                                    <div class="pl-lg-4">
                                        <div class="form-group{{ $errors->has('full_name') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-full-name">{{ __('Full Name') }}</label>
                                            <input type="text" name="full_name" id="input-full-name" class="form-control form-control-alternative{{ $errors->has('full_name') ? ' is-invalid' : '' }}" placeholder="{{ __('Full Name') }}" value="{{ old('full_name', $user->full_name) }}" autofocus>

                                            @if ($errors->has('full_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('full_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                            <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Name') }}" value="{{ old('name', auth()->user()->name) }}" required>

                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-email">{{ __('Email') }}</label>
                                            <input type="email" name="email" id="input-email" class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" value="{{ old('email', auth()->user()->email) }}" required>

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('birth_date') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-birth-date">{{ __('Birth Date') }}</label>
                                            <input type="date" name="birth_date" id="input-birth-date" class="form-control form-control-alternative{{ $errors->has('birth_date') ? ' is-invalid' : '' }}" placeholder="{{ __('Birth Date') }}" value="{{ old('birth_date', $user->birth_date) }}">

                                            @if ($errors->has('birth_date'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('birth_date') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('gender') ? ' has-danger' : '' }}">
                                            <legend class="col-form-label">Gender</legend>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="gender" id="male" value="Male" {{ ($user->gender == "Male") ? 'checked' : '' }}>
                                                <label class="form-check-label" for="male">
                                                  Male
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="gender" id="female" value="Female" {{ ($user->gender == "Female") ? 'checked' : '' }}>
                                                <label class="form-check-label" for="female">
                                                  Female
                                                </label>
                                            </div>

                                            @if ($errors->has('gender'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('gender') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-address">Address</label>
                                            <textarea name="address" class="form-control" id="input-address" rows="3">{{ old('address', $user->address) }}</textarea>

                                            @if ($errors->has('address'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('phone_number') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-phone-number">{{ __('Phone Number') }}</label>
                                            <input type="text" name="phone_number" id="input-phone-number" class="form-control form-control-alternative{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" value="{{ old('phone_number', $user->phone_number) }}">

                                            @if ($errors->has('phone_number'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('hobby') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-hobby">{{ __('Hobby') }}</label>
                                            <input type="text" name="hobby" id="input-hobby" class="form-control form-control-alternative{{ $errors->has('hobby') ? ' is-invalid' : '' }}" placeholder="{{ __('Hobby') }}" value="{{ old('hobby', $user->hobby) }}">

                                            @if ($errors->has('hobby'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('hobby') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="custom-file form-group{{ $errors->has('image') ? ' has-danger' : '' }}">
                                            <input type="file" name="image" class="custom-file-input" id="input-image" lang="en">
                                            <label class="custom-file-label" for="input-image">Select file</label>
                                            <p>
                                                <small class="text-danger">For best result, please use image size 800x800</small>
                                            </p>
                                            @if ($user->image)
                                                <img src="/image/user/profile/{{ $user->image }}" alt="Profile Image" width="150" class="mt-2">
                                            @endif

                                            @if ($errors->has('image'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="text-center mt-8">
                                            <button type="submit" class="btn btn-primary mt-4">{{ __('Save') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            {{-- Basic Info Tab --}}

                            {{-- Education Tab --}}
                            <div class="tab-pane fade" id="education" role="tabpanel" aria-labelledby="education-tab">
                                <div class="row mb-4">
                                    <div class="col-md-8">
                                        <h6 class="heading-small text-muted">{{ __('User Educations') }}</h6>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addEducation">Add Education</button>
                                    </div>
                                </div>

                                @forelse ($userEducations as $education)
                                    <div class="row mb-3">
                                        <div class="col-md-2 text-center">
                                            <img src="{{ asset('assets/img/icons/user/Graduation Cap.svg') }}" width="80" alt="icon education">
                                        </div>
                                        <div class="col-md-8">
                                            <h5 class="text-muted">{{ date('Y', strtotime($education->start_period)) }} - {{ date('Y', strtotime($education->end_period)) }}</h5>
                                            <h3>{{ $education->major }} at {{ $education->school_name }}</h3>
                                            <h4>{{ $education->degree }}</h4>
                                            <p class="text-muted">{{ $education->gpa }} / {{ $education->gpa_max }}</p>
                                        </div>
                                        <div class="col-md-2">
                                            <a href="javascript:void(0)" id="show-education" class="btn btn-outline-secondary btn-sm edit" data-id="{{ $education->id }}">
                                                Edit
                                            </a>
                                            <a href="javascript:;;" class="btn btn-danger btn-sm" onclick="deleteEducationModal({{ $education->id }})">Delete</a>
                                        </div>
                                    </div>
                                @empty
                                    <div class="row mb-3">
                                        <div class="col-md-12 text-center">
                                            <p class="description">There is no educations</p>
                                        </div>
                                    </div>
                                @endforelse
                            </div>
                            {{-- Education Tab --}}

                            {{-- Experience Tab --}}
                            <div class="tab-pane fade" id="experience" role="tabpanel" aria-labelledby="experience-tab">
                                <div class="row mb-4">
                                    <div class="col-md-8">
                                        <h6 class="heading-small text-muted">{{ __('User Experiences') }}</h6>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addExperience">Add Experience</button>
                                    </div>
                                </div>

                                @forelse ($userExperiences as $experience)
                                    <div class="row mb-3">
                                        <div class="col-md-2 text-center">
                                            <img src="{{ asset('assets/img/icons/user/Project Management.svg') }}" width="80" alt="icon experience">
                                        </div>
                                        <div class="col-md-8">
                                            <h5 class="text-muted">{{ date('Y', strtotime($experience->start_period)) }} - {{ date('Y', strtotime($experience->end_period)) }}</h5>
                                            <h3>{{ $experience->position_name }} at {{ $experience->company_name }}</h3>
                                            <p class="text-muted">{{ $experience->working_description }}</p>
                                        </div>
                                        <div class="col-md-2">
                                            <a href="javascript:void(0)" id="show-experience" class="btn btn-outline-secondary btn-sm edit" data-id="{{ $experience->id }}">
                                                Edit
                                            </a>
                                            <a href="javascript:;;" class="btn btn-danger btn-sm" onclick="deleteExperienceModal({{ $experience->id }})">Delete</a>
                                        </div>
                                    </div>
                                @empty
                                    <div class="row mb-3">
                                        <div class="col-md-12 text-center">
                                            <p class="description">There is no experiences</p>
                                        </div>
                                    </div>
                                @endforelse
                            </div>
                            {{-- Experience Tab --}}

                            {{-- Document Tab --}}
                            <div class="tab-pane fade" id="document" role="tabpanel" aria-labelledby="document-tab">
                                <div class="row mb-4">
                                    <div class="col-md-8">
                                        <h6 class="heading-small text-muted">{{ __('User Docouments') }}</h6>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addDocument">Add Document</button>
                                    </div>
                                </div>

                                @forelse ($userDocuments as $document)
                                    <div class="row mb-3">
                                        <div class="col-md-2 text-center">
                                            <img src="{{ asset('assets/img/icons/user/PDF.svg') }}" width="80" alt="icon document">
                                        </div>
                                        <div class="col-md-8">
                                            <h3>{{ $document->document_name }}</h3>
                                            <a href="/file/user/profile/{{ $document->file }}" target="_blank">View Document</a>
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <a href="javascript:;;" class="btn btn-danger btn-sm" onclick="deleteDocumentModal({{ $document->id }})">Delete</a>
                                        </div>
                                    </div>
                                @empty
                                    <div class="row mb-3">
                                        <div class="col-md-12 text-center">
                                            <p class="description">There is no documents</p>
                                        </div>
                                    </div>
                                @endforelse
                            </div>
                            {{-- Document Tab --}}
                        </div>
                    </div>
                </div>
            </div>

            {{-- Modal Add Education --}}
            <div class="modal fade" id="addEducation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title" id="exampleModalLabel">Add Education</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form role="form" method="POST" action="{{ route('user.profile.education.store', auth()->user()->id) }}">
                                @csrf
                                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

                                <div class="form-group">
                                    <label class="form-control-label" for="input-school-name">School / University</label>
                                    <input type="text" name="school_name" id="input-school-name" class="form-control form-control-alternative" autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="input-degree" class="form-control-label">Degree</label>
                                    <select name="degree" id="input-degree" class="form-control">
                                        <option value="Associate Degree (D3)">Associate Degree (D3)</option>
                                        <option value="Bachelor Degree (S1)">Bachelor Degree (S1)</option>
                                        <option value="Master Degree (S2)">Master Degree (S2)</option>
                                        <option value="Doctoral Degree (S3)">Doctoral Degree (S3)</option>
                                        <option value="High School (SMP/SMA)">High School (SMP/SMA)</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="input-major">Major</label>
                                    <input type="text" name="major" id="input-major" class="form-control form-control-alternative">
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="input-gpa">GPA / Score</label>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <input type="number" step="0.01" class="form-control form-control-alternative" name="gpa" id="input-gpa">
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="text-muted">of</span>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="number" step="0.01" class="form-control form-control-alternative" name="gpa_max" id="input-gpa">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="input-start-period">Period</label>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <input type="date" name="start_period" id="input-start-period" class="form-control form-control-alternative">
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="text-muted">to</span>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="date" name="end_period" id="input-start-period" class="form-control form-control-alternative">
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="button" class="btn btn-secondary mt-4" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary mt-4">{{ __('Save') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Modal Add Education --}}

            {{-- Modal Edit Education --}}
            <div class="modal fade" id="editEducation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title" id="exampleModalLabel">Edit Education</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                @csrf
                                <input type="hidden" name="id" id="education_id">

                                <div class="form-group">
                                    <label class="form-control-label" for="input-school-name-edit">School / University</label>
                                    <input type="text" name="school_name" id="input-school-name-edit" class="form-control form-control-alternative" autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="input-degree-edit" class="form-control-label">Degree</label>
                                    <select name="degree" id="input-degree-edit" class="form-control">
                                        <option value="Associate Degree (D3)">Associate Degree (D3)</option>
                                        <option value="Bachelor Degree (S1)">Bachelor Degree (S1)</option>
                                        <option value="Master Degree (S2)">Master Degree (S2)</option>
                                        <option value="Doctoral Degree (S3)">Doctoral Degree (S3)</option>
                                        <option value="High School (SMP/SMA)">High School (SMP/SMA)</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="input-major-edit">Major</label>
                                    <input type="text" name="major" id="input-major-edit" class="form-control form-control-alternative">
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="input-gpa">GPA / Score</label>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <input type="number" step="0.01" class="form-control form-control-alternative" name="gpa" id="input-gpa-edit">
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="text-muted">of</span>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="number" step="0.01" class="form-control form-control-alternative" name="gpa_max" id="input-gpa-max-edit">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="input-start-period">Period</label>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <input type="date" name="start_period" id="input-start-period-edit" class="form-control form-control-alternative">
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="text-muted">to</span>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="date" name="end_period" id="input-end-period-edit" class="form-control form-control-alternative">
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="button" class="btn btn-secondary mt-4" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary mt-4" id="updateEducation">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Modal Edit Education --}}

            {{-- Modal Delete Education --}}
            <div class="modal fade" id="deleteEducationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <form id="delete-education" class="d-none" action="" method="post">
                        @csrf
                        @method('delete')

                    </form>
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Delete Education</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this item?
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                      <button type="button" class="btn btn-danger" onclick="$('#delete-education').submit()">Yes</button>
                    </div>
                  </div>
                </div>
            </div>
            {{-- Modal Delete Education --}}

            {{-- Modal Add Experience --}}
            <div class="modal fade" id="addExperience" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title" id="exampleModalLabel">Add Experience</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form role="form" method="POST" action="{{ route('user.profile.experience.store', auth()->user()->id) }}">
                                @csrf
                                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

                                <div class="form-group">
                                    <label class="form-control-label" for="input-company-name">Company Name</label>
                                    <input type="text" name="company_name" id="input-company-name" class="form-control form-control-alternative" autofocus>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="input-position-name">Position Name</label>
                                    <input type="text" name="position_name" id="input-position-name" class="form-control form-control-alternative">
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="input-position-level">Position Level</label>
                                    <input type="text" name="position_level" id="input-position-level" class="form-control form-control-alternative">
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="input-start-period">Period</label>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <input type="date" name="start_period" id="input-start-period" class="form-control form-control-alternative">
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="text-muted">to</span>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="date" name="end_period" id="input-start-period" class="form-control form-control-alternative">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="input-working-description">Working Description</label>
                                    <textarea name="working_description" class="form-control" id="input-working-description" rows="3"></textarea>
                                </div>

                                <div class="text-center">
                                    <button type="button" class="btn btn-secondary mt-4" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary mt-4">{{ __('Save') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Modal Add Experience --}}

            {{-- Modal Edit Experience --}}
            <div class="modal fade" id="editExperience" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title" id="exampleModalLabel">Edit Experience</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form role="form">
                                @csrf
                                <input type="hidden" name="id" id="experience_id">

                                <div class="form-group">
                                    <label class="form-control-label" for="input-company-name-edit">Company Name</label>
                                    <input type="text" name="company_name" id="input-company-name-edit" class="form-control form-control-alternative" autofocus>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="input-position-name-edit">Position Name</label>
                                    <input type="text" name="position_name" id="input-position-name-edit" class="form-control form-control-alternative">
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="input-position-level-edit">Position Level</label>
                                    <input type="text" name="position_level" id="input-position-level-edit" class="form-control form-control-alternative">
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="input-start-period">Period</label>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <input type="date" name="start_period" id="input-experience-start-period-edit" class="form-control form-control-alternative">
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="text-muted">to</span>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="date" name="end_period" id="input-experience-end-period-edit" class="form-control form-control-alternative">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="input-working-description-edit">Working Description</label>
                                    <textarea name="working_description" class="form-control" id="input-working-description-edit" rows="3"></textarea>
                                </div>

                                <div class="text-center">
                                    <button type="button" class="btn btn-secondary mt-4" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary mt-4" id="updateExperience">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Modal Edit Experience --}}

            {{-- Modal Delete Experience --}}
            <div class="modal fade" id="deleteExperienceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <form id="delete-experience" class="d-none" action="" method="post">
                        @csrf
                        @method('delete')

                    </form>
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Delete Experience</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this item?
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                      <button type="button" class="btn btn-danger" onclick="$('#delete-experience').submit()">Yes</button>
                    </div>
                  </div>
                </div>
            </div>
            {{-- Modal Delete Experience --}}

            {{-- Modal Add Document --}}
            <div class="modal fade" id="addDocument" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title" id="exampleModalLabel">Add Document</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form role="form" method="POST" action="{{ route('user.profile.document.store', auth()->user()->id) }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

                                <div class="form-group">
                                    <label class="form-control-label" for="input-document-name">Document Name</label>
                                    <input type="text" name="document_name" id="input-document-name" class="form-control form-control-alternative" autofocus>
                                </div>

                                <div class="custom-file form-group{{ $errors->has('file') ? ' has-danger' : '' }}">
                                    <input type="file" name="file" class="custom-file-input" id="input-file" lang="en">
                                    <label class="custom-file-label" for="input-file">Select file</label>

                                    @if ($errors->has('file'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="text-right">
                                    <button type="button" class="btn btn-secondary mt-4" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary mt-4">{{ __('Save') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Modal Add Document --}}

            {{-- Modal Delete Document --}}
            <div class="modal fade" id="deleteDocumentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <form id="delete-document" class="d-none" action="" method="post">
                        @csrf
                        @method('delete')

                    </form>
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Delete Document</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this item?
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                      <button type="button" class="btn btn-danger" onclick="$('#delete-document').submit()">Yes</button>
                    </div>
                  </div>
                </div>
            </div>
            {{-- Modal Delete Document --}}
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Edit Education
        $("#show-education").click(function () {
            var id = $(this).data('id');
            var route = "education/edit/" + id;

            $.ajax({
                type: "GET",
                url: route,
                success: function (data) {
                    console.log(data);
                    $('#education_id').val(data.id);
                    $('#input-school-name-edit').val(data.school_name);
                    $('#input-degree-edit').val(data.degree);
                    $('#input-major-edit').val(data.major);
                    $('#input-gpa-edit').val(data.gpa);
                    $('#input-gpa-max-edit').val(data.gpa_max);
                    $('#input-start-period-edit').val(data.start_period);
                    $('#input-end-period-edit').val(data.end_period);
                    $('#editEducation').modal('show');
                }
            });
        });

        // Update Education
        $("#updateEducation").click(function () {
            var id = $('#education_id').val();
            var school_name = $('#input-school-name-edit').val();
            var degree = $('#input-degree-edit').val();
            var major = $('#input-major-edit').val();
            var gpa = $('#input-gpa-edit').val();
            var gpa_max = $('#input-gpa-max-edit').val();
            var start_period = $('#input-start-period-edit').val();
            var end_period= $('#input-end-period-edit').val();
            var route = "education/update/" + id;

            $.ajax({
                type: "POST",
                url: route,
                data: {
                    id: id,
                    school_name: school_name,
                    degree: degree,
                    major: major,
                    gpa: gpa,
                    gpa_max: gpa_max,
                    start_period: start_period,
                    end_period: end_period
                },
                success: function (data) {
                    window.location.reload();
                }
            });
        });

        // Edit Experience
        $("#show-experience").click(function () {
            var id = $(this).data('id');
            var route = "experience/edit/" + id;

            $.ajax({
                type: "GET",
                url: route,
                success: function (data) {
                    console.log(data);
                    $('#experience_id').val(data.id);
                    $('#input-company-name-edit').val(data.company_name);
                    $('#input-position-name-edit').val(data.position_name);
                    $('#input-position-level-edit').val(data.position_level);
                    $('#input-experience-start-period-edit').val(data.start_period);
                    $('#input-experience-end-period-edit').val(data.end_period);
                    $('#input-working-description-edit').val(data.working_description);
                    $('#editExperience').modal('show');
                }
            });
        });

        // Update Education
        $("#updateExperience").click(function () {
            var id = $('#experience_id').val();
            var company_name = $('#input-company-name-edit').val();
            var position_name = $('#input-position-name-edit').val();
            var position_level = $('#input-position-level-edit').val();
            var start_period = $('#input-experience-start-period-edit').val();
            var end_period= $('#input-experience-end-period-edit').val();
            var working_description = $('#input-working-description-edit').val();
            var route = "experience/update/" + id;

            $.ajax({
                type: "POST",
                url: route,
                data: {
                    id: id,
                    company_name: company_name,
                    position_name: position_name,
                    position_level: position_level,
                    start_period: start_period,
                    end_period: end_period,
                    working_description: working_description
                },
                success: function (data) {
                    window.location.reload();
                }
            });
        });

        // Edit Document
        $("#show-document").click(function () {
            var id = $(this).data('id');
            var route = "document/edit/" + id;

            $.ajax({
                type: "GET",
                url: route,
                success: function (data) {
                    $('#document_id').val(data.id);
                    $('#input-document-name-edit').val(data.document_name);
                    $('#view_document').attr('href', 'file/user/profile/'+data.file);
                    $('#editDocument').modal('show');
                }
            });
        });
    })

    function deleteEducationModal(id) {
        $('#delete-education').attr('action', '{{url('user/profile/education/delete/')}}/'+id)
        $('#deleteEducationModal').modal('show')
    }

    function deleteExperienceModal(id) {
        $('#delete-experience').attr('action', '{{url('user/profile/experience/delete/')}}/'+id)
        $('#deleteExperienceModal').modal('show')
    }

    function deleteDocumentModal(id) {
        $('#delete-document').attr('action', '{{url('user/profile/document/delete/')}}/'+id)
        $('#deleteDocumentModal').modal('show')
    }
</script>
@endsection
