@extends('public.user.layouts.default')

@section('content')
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-md-8">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Active Transactions</h3>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Position</th>
                                <th scope="col">Status</th>
                                <th scope="col">Applied Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Backend Engineer</td>
                                <td>
                                    <span class="badge badge-pill badge-warning">Waiting</span>
                                </td>
                                <td>12/02/2020 11:00</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">

                    </nav>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Inactive</h3>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Position</th>
                                <th scope="col">Status</th>
                                <th scope="col">Updated Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Backend Engineer</td>
                                <td>
                                    <span class="badge badge-pill badge-danger">Rejected</span>
                                </td>
                                <td>12/02/2020 11:00</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">

                    </nav>
                </div>
            </div>
        </div>
    </div>

    {{-- Detail Modal --}}
    <div class="modal fade" id="detailJob" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLabel">Finance Manager</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row border">
                        <div class="col-sm border">
                            <h4>Divition: Finance</h4>
                        </div>
                        <div class="col-sm border">
                            <h4>Level: Manager</h4>
                        </div>
                        <div class="col-sm border">
                            <h4>Location: Jakarta</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm text-right">
                            <small class="text-muted">Applications are open until 31 December 2021</small>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-sm-12">
                            <h4>Description</h4>
                            <p class="text-muted">
                                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Recusandae possimus vel expedita eveniet quibusdam corporis veniam iure totam quasi tempore maiores, illum impedit harum beatae maxime perspiciatis. Perferendis, temporibus neque!
                            </p>
                        </div>
                        <div class="col-sm-12 mt-3">
                            <h4>Job Requirement</h4>
                            <p class="text-muted">
                                <ul class="text-muted">
                                    <li>
                                        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ratione error neque expedita nesciunt porro harum beatae, in eligendi? Maiores quia ea, ducimus nisi veritatis assumenda necessitatibus corporis iste quas culpa.
                                    </li>
                                    <li>
                                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quo alias iste, atque explicabo amet voluptates saepe quod provident nostrum laborum corporis rem odio perferendis. Fugiat quidem nobis ut rem similique.
                                    </li>
                                    <li>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda nihil totam vel odit temporibus! Blanditiis quae ad, aspernatur eos dolorem facere esse vel quas officiis neque doloremque, animi corrupti iure!
                                    </li>
                                    <li>
                                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem error, deserunt possimus suscipit atque nostrum ab harum voluptatum dignissimos. Repellat reprehenderit repellendus iure recusandae unde reiciendis molestiae ad, praesentium officiis.
                                    </li>
                                    <li>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime dolorem facilis ullam tempora reprehenderit rem, quisquam, fugiat veritatis quam temporibus, molestiae aliquid aspernatur a. Quis autem ipsa odit iste placeat!
                                    </li>
                                </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success">Apply</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Detail Modal --}}
</div>
@endsection
