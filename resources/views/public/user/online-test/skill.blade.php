@extends('public.user.layouts.default')

@section('content')
<div class="container-fluid mt--7">
    <div class="row mb-5">
        <div class="col-md-12">
            <div class="card shadow p-5 text-center">
                <h3 class="mb-0">Hey {{ auth()->user()->name }},</h3>
                <h2 class="mt-1">
                    <strong>
                        Welcome to Intrack Market Service Skill Test
                    </strong>
                </h2>
                <p class="mt-3">
                    <span class="badge badge-info p-2"><i class="fas fa-clock mr-2"></i> Test Duration : 10 Mins</span>
                    <span class="badge badge-info p-2"><i class="fas fa-question-circle mr-2"></i> No. of Question : 10 questions</span>
                </p>

                @if (session('skillSuccessMessage'))
                    <div class="alert alert-success" role="alert">
                      <h4 class="alert-heading">Skill Test</h4>
                      <p></p>
                      <p class="mb-0">{{ session('skillSuccessMessage') }}</p>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <a href="{{ route('user.dashboard') }}" class="btn btn-primary btn-sm">Back To Home</a>
                        </div>
                    </div>
                @else
                    <button type="button" onclick="showTest()" id="btnShowTest" class="btn btn-danger">Start Test</button>
                @endif
            </div>
        </div>
    </div>

    <div class="row" id="testCard" hidden>
        <div class="col-md-8">
            <form action="{{ route('user.exam.skill.store', ['applicant_id' => $applicant_id]) }}" method="POST" id="formTest">
                @csrf

                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Skill Test</h3>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @foreach ($skills as $number => $test)
                            <div class="row mb-3">
                                <div class="col-8">
                                    <h2 class="mb-0">{{ $number + 1 . '. ' . $test->question }}</h2>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-6">
                                    <input type="hidden" name="user_applicant_id" value="{{ $applicant_id }}">
                                    <input type="hidden" name="skill_test_id[]" value="{{ $test->id }}">

                                    <div class="custom-control custom-control-alternative custom-radio mb-3">
                                        <input name="answer_[{{ $test->id }}]" class="custom-control-input" id="{{ $test->id }}answer_1" value="{{ $test->answer_1 }}" type="radio">
                                        <label class="custom-control-label" for="{{ $test->id }}answer_1">{{ $test->answer_1 }}</label>
                                    </div>
                                    <div class="custom-control custom-control-alternative custom-radio mb-3">
                                        <input name="answer_[{{ $test->id }}]" class="custom-control-input" id="{{ $test->id }}answer_2" value="{{ $test->answer_2 }}" type="radio">
                                        <label class="custom-control-label" for="{{ $test->id }}answer_2">{{ $test->answer_2 }}</label>
                                    </div>
                                    <div class="custom-control custom-control-alternative custom-radio mb-3">
                                        <input name="answer_[{{ $test->id }}]" class="custom-control-input" id="{{ $test->id }}answer_3" value="{{ $test->answer_3 }}" type="radio">
                                        <label class="custom-control-label" for="{{ $test->id }}answer_3">{{ $test->answer_3 }}</label>
                                    </div>
                                    <div class="custom-control custom-control-alternative custom-radio mb-3">
                                        <input name="answer_[{{ $test->id }}]" class="custom-control-input" id="{{ $test->id }}answer_4" value="{{ $test->answer_4 }}" type="radio">
                                        <label class="custom-control-label" for="{{ $test->id }}answer_4">{{ $test->answer_4 }}</label>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="card-footer py-4">
                        <button type="submit" id="btnSubmitTest" class="btn btn-success">Submit Test</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-4">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Timer</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <span id="timer">15:00</span> Minutes
                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">

                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    function showTest() {
        $('#testCard').removeAttr('hidden');
        $('#btnShowTest').attr('hidden', true);
        var fiveMinutes = 60 * 10,
            display = document.querySelector('#timer');
        startTimer(fiveMinutes, display);
    }

    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.textContent = minutes + ":" + seconds;

            if (--timer < 0) {
                document.getElementById("formTest").submit();
                timer = duration;
            }
        }, 1000);
    }


</script>
@endsection
