@extends('public.user.layouts.default')

@section('content')
<div class="container-fluid mt--6">
    <div class="row">
        @if ($userApplied)
            <div class="col-md-12">
                <div class="card shadow p-5">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3 class="display-4">Apply job failed</h3>
                            <p class="mt-5">
                                You have been apply for a job. <br> Please wait until you finished the recruitment process.
                            </p>
                        </div>
                        <div class="col-md-12 text-center mt-5">
                            <a href="{{ route('user.dashboard') }}" class="btn btn-primary">Back To Home</a>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($isUserFilledData == false)
            <div class="col-md-12">
                <div class="card shadow p-5">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3 class="display-4">Apply job failed</h3>
                            <p class="mt-5">
                                Please complete your profile before apply a job.
                            </p>
                        </div>
                        <div class="col-md-12 text-center mt-5">
                            <a href="{{ route('user.dashboard') }}" class="btn btn-primary">Back To Home</a>
                            <a href="{{ route('user.profile', auth()->user()->id) }}" class="btn btn-secondary">Go To Profile Page</a>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="col-md-12">
                <div class="card shadow p-5">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3 class="display-4">Submit this job application ?</h3>
                            <img src="{{ $job->division->division_icon_link }}" alt="Division Icon" class="mt-3" width="150">
                            <p class="mt-5">
                                Your profile will be submitted with your application for the <strong>{{ $job->job_name }}</strong> position
                            </p>
                        </div>
                        <div class="col-md-12 text-center mt-5">
                            <a href="{{ route('user.job.apply.store', ['user_id' => auth()->user()->id, 'job_id' => $job->id]) }}" class="btn btn-success">Submit Application</a>
                            <a href="{{ route('user.profile', ['id' => auth()->user()->id]) }}" class="btn btn-secondary">View My Profile</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection
