@extends('public.user.layouts.default')

@section('content')
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow p-5">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="btn-inner--icon mr-5">
                                    <i class="ni ni-archive-2"></i>
                                </span>
                                All Category
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach ($divisions as $division)
                                    <a class="dropdown-item" href="{{ route('user.job.filter', $division->id) }}">{{ $division->name }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <form class="navbar-search" method="get" action="{{ route('job.search') }}" id="search_jobs">
                            <div class="form-group mb-0">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Type Your Keywords" type="text" name="q" value="{{ $q ?? '' }}" id="keyword_jobs">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    {{-- Job List --}}
    @foreach ($divisions as $division)
        @if (count($division->jobs) > 0)
            <div id="division" class="division-card-{{ $division->id }}">
                <div class="row mt-4 mb-5 justify-content-end" @if(Route::currentRouteName() === 'user.job') hidden @endif>
                    <div class="col-md-2">
                        <a href="{{ route('user.job') }}" class="badge badge-pill badge-warning"><i class="ni ni-fat-remove"></i> Clear Filter</a>
                    </div>
                </div>

                <div class="row mt-6">
                    <div class="col-md-2 text-center">
                        <img src="{{ $division->division_icon_link }}" width="100" alt="Card image category finance">
                    </div>
                    <div class="col-md-10">
                        <h2>{{ ucwords($division->name) }}</h2>
                        <p class="text-muted">{{ $division->description }}</p>
                    </div>
                </div>

                <div class="row mt-3">
                    @foreach ($division->jobs as $job)
                        <div class="col-md-4">
                            <div class="card job-list-card">
                                <div class="card-body">
                                    <h5 class="card-title">{{ ucwords($job->job_name) }}</h5>
                                    <small class="card-text">{{ $job->job_short_description }}</small>
                                    <a href="javascript:void(0)" id="showJobDetail" data-id="{{ $job->id }}" class="btn btn-success btn-block mt-3 showJobDetail">See Details</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <hr class="my-5">
            </div>
        @endif
    @endforeach
    {{-- End Job List --}}

    {{-- Detail Modal --}}
    <div class="modal fade" id="showJobDetailModal" tabindex="-1" role="dialog" aria-labelledby="job_name" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title" id="job_name"></h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row border">
                        <div class="col-sm border">
                            <h4 id="division_name"></h4>
                        </div>
                        <div class="col-sm border">
                            <h4 id="job_level"></h4>
                        </div>
                        <div class="col-sm border">
                            <h4 id="job_location"></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm text-right">
                            <small class="text-muted" id="job_period_date"></small>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-sm-12">
                            <h4>Description</h4>
                            <p class="text-muted" id="job_description"></p>
                        </div>
                        <div class="col-sm-12 mt-3">
                            <h4>Job Requirement</h4>
                            <p class="text-muted" id="job_requirement"></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="job_id">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <a id="applyJobUrl" class="btn btn-success">Apply</a>
                </div>
            </div>
        </div>
    </div>
    {{-- Detail Modal --}}
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".showJobDetail").click(function () {
            var id = $(this).data('id');
            var route = "job/detail/" + id;

            $.ajax({
                type: "GET",
                url: route,
                success: function (data) {
                    // console.log(data);
                    $('#applyJobUrl').attr("href", "job/apply/" + data.id);
                    $('#job_id').val(data.id);
                    $('#division_name').text("Division : " + data.division_name);
                    $('#job_name').text(data.job_name);
                    $('#job_level').text("Level : " + data.level_name);
                    $('#job_location').text("Location : " + data.job_location);
                    $('#job_period_date').text("Applications are open until " + data.job_period_date);
                    $('#job_short_description').val(data.job_short_description);
                    $('#job_description').html(data.job_description);
                    $('#job_requirement').html(data.job_requirement);
                    $('#showJobDetailModal').modal('show');
                }
            });
        });

        $('#keyword_jobs').keypress(function (e) {
            var key = e.which;
            if (key == 13) {
                $('#search_jobs').submit();
            }
        })
    })
</script>
@endsection
