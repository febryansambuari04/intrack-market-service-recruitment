@extends('public.user.layouts.default')

@section('content')
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow p-5">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3 class="display-4">Application success!</h3>
                        <p class="mt-5">
                            Your application for <strong>{{ $job->job_name }}</strong> has been submitted. <br>
                            We'll notify you about the outcome of your aplication within 2 weeks.
                        </p>
                        <img src="{{ asset('assets/img/illustrations/bg-success-apply-job.svg') }}" alt="Division Icon" class="mt-1" width="500">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center">
                        <a href="{{ route('user.dashboard') }}" class="btn btn-primary btn-sm">Back To Home</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
