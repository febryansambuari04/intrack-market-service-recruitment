<?php

use App\Http\Controllers\Admin\AdminDashboardController;
use App\Http\Controllers\Admin\AdminExercisePsychologicalTestController;
use App\Http\Controllers\Admin\AdminExerciseSkillTestController;
use App\Http\Controllers\Admin\AdminManagementController;
use App\Http\Controllers\Admin\AdminUserController;
use App\Http\Controllers\Admin\AdminUserDocumentController;
use App\Http\Controllers\Admin\AdminUserEducationController;
use App\Http\Controllers\Admin\AdminUserExperienceController;
use App\Http\Controllers\Admin\AdminUserStatusController;
use App\Http\Controllers\Admin\AdminVacancyDivisionController;
use App\Http\Controllers\Admin\AdminVacancyJobController;
use App\Http\Controllers\Admin\AdminVacancyLevelController;
use App\Http\Controllers\Admin\Auth\LoginController;
use Illuminate\Support\Facades\Route;

Route::prefix('admin')->as('admin.')->group(function () {
    Route::get('login', [LoginController::class, 'getLogin'])->name('login');
    Route::post('login', [LoginController::class, 'postLogin'])->name('login.post');
    Route::get('logout', [LoginController::class, 'logout'])->name('logout');

    Route::middleware(['adminauth'])->group(function () {
        Route::get('/dashboard', [AdminDashboardController::class, 'index'])->name('dashboard');
        Route::get('/profile', [AdminDashboardController::class, 'profile'])->name('profile');
        Route::post('/profile/update', [AdminDashboardController::class, 'updateProfile'])->name('profile.update');
        Route::post('/profile/password/update', [AdminDashboardController::class, 'updateProfilePassword'])->name('profile.password.update');

        Route::prefix('management')->as('management.')->group(function () {
            Route::get('/', [AdminManagementController::class, 'index'])->name('index');
            Route::get('/create', [AdminManagementController::class, 'create'])->name('create');
            Route::post('/store', [AdminManagementController::class, 'store'])->name('store');
            Route::delete('/delete/{id}', [AdminManagementController::class, 'destroy'])->name('delete');
        });

        Route::prefix('user')->as('user.')->group(function () {
            Route::get('/', [AdminUserController::class, 'index'])->name('index');
            Route::get('/detail/{id}', [AdminUserController::class, 'show'])->name('show');
            Route::delete('/delete/{id}', [AdminUserController::class, 'deleteUser'])->name('delete');
            Route::get('/export', [AdminUserController::class, 'exportUser'])->name('export');

            Route::prefix('education')->as('education.')->group(function () {
                Route::get('/{user_id}', [AdminUserEducationController::class, 'index'])->name('index');
                Route::delete('/delete/{id}', [AdminUserEducationController::class, 'deleteEducation'])->name('delete');
            });

            Route::prefix('experience')->as('experience.')->group(function () {
                Route::get('/{user_id}', [AdminUserExperienceController::class, 'index'])->name('index');
                Route::delete('/delete/{id}', [AdminUserExperienceController::class, 'deleteExperience'])->name('delete');
            });

            Route::prefix('status')->as('status.')->group(function () {
                Route::get('/', [AdminUserStatusController::class, 'index'])->name('index');
                Route::get('/edit/{id}', [AdminUserStatusController::class, 'edit'])->name('edit');
                Route::post('/update/{id}', [AdminUserStatusController::class, 'update'])->name('update');
                Route::delete('/delete/{id}', [AdminUserStatusController::class, 'delete'])->name('delete');
                Route::get('/export', [AdminUserStatusController::class, 'exportUserStatus'])->name('export');
                Route::get('/view-test/{id}', [AdminUserStatusController::class, 'viewTest'])->name('view-test');
                Route::get('/view-data/{id}', [AdminUserStatusController::class, 'viewData'])->name('view-data');
            });

            Route::prefix('document')->as('document.')->group(function () {
                Route::get('/{user_id}', [AdminUserDocumentController::class, 'index'])->name('index');
                Route::delete('/delete/{id}', [AdminUserDocumentController::class, 'deleteDocument'])->name('delete');
            });
        });

        Route::prefix('vacancy')->as('vacancy.')->group(function () {
            Route::prefix('division')->as('division.')->group(function () {
                Route::get('/', [AdminVacancyDivisionController::class, 'index'])->name('index');
                Route::get('/create', [AdminVacancyDivisionController::class, 'create'])->name('create');
                Route::post('/store', [AdminVacancyDivisionController::class, 'store'])->name('store');
                Route::get('/edit/{id}', [AdminVacancyDivisionController::class, 'edit'])->name('edit');
                Route::post('/update/{id}', [AdminVacancyDivisionController::class, 'update'])->name('update');
                Route::delete('/delete/{id}', [AdminVacancyDivisionController::class, 'destroy'])->name('destroy');
            });

            Route::prefix('level')->as('level.')->group(function () {
                Route::get('/', [AdminVacancyLevelController::class, 'index'])->name('index');
                Route::get('/create', [AdminVacancyLevelController::class, 'create'])->name('create');
                Route::post('/store', [AdminVacancyLevelController::class, 'store'])->name('store');
                Route::get('/edit/{id}', [AdminVacancyLevelController::class, 'edit'])->name('edit');
                Route::post('/update/{id}', [AdminVacancyLevelController::class, 'update'])->name('update');
                Route::delete('/delete/{id}', [AdminVacancyLevelController::class, 'destroy'])->name('destroy');
            });

            Route::prefix('job')->as('job.')->group(function () {
                Route::get('/', [AdminVacancyJobController::class, 'index'])->name('index');
                Route::get('/detail/{id}', [AdminVacancyJobController::class, 'show'])->name('show');
                Route::get('/create', [AdminVacancyJobController::class, 'create'])->name('create');
                Route::post('/store', [AdminVacancyJobController::class, 'store'])->name('store');
                Route::get('/edit/{id}', [AdminVacancyJobController::class, 'edit'])->name('edit');
                Route::post('/update/{id}', [AdminVacancyJobController::class, 'update'])->name('update');
                Route::delete('/delete/{id}', [AdminVacancyJobController::class, 'destroy'])->name('destroy');
            });
        });

        Route::prefix('exercise')->as('exercise.')->group(function () {
            Route::prefix('psychological')->as('psychological.')->group(function () {
                Route::get('/', [AdminExercisePsychologicalTestController::class, 'index'])->name('index');
                Route::get('/detail/{id}', [AdminExercisePsychologicalTestController::class, 'show'])->name('show');
                Route::get('/create', [AdminExercisePsychologicalTestController::class, 'create'])->name('create');
                Route::post('/store', [AdminExercisePsychologicalTestController::class, 'store'])->name('store');
                Route::get('/edit/{id}', [AdminExercisePsychologicalTestController::class, 'edit'])->name('edit');
                Route::post('/update/{id}', [AdminExercisePsychologicalTestController::class, 'update'])->name('update');
                Route::delete('/delete/{id}', [AdminExercisePsychologicalTestController::class, 'destroy'])->name('destroy');
            });

            Route::prefix('skill')->as('skill.')->group(function () {
                Route::get('/', [AdminExerciseSkillTestController::class, 'index'])->name('index');
                Route::get('/detail/{id}', [AdminExerciseSkillTestController::class, 'show'])->name('show');
                Route::get('/create', [AdminExerciseSkillTestController::class, 'create'])->name('create');
                Route::post('/store', [AdminExerciseSkillTestController::class, 'store'])->name('store');
                Route::get('/edit/{id}', [AdminExerciseSkillTestController::class, 'edit'])->name('edit');
                Route::post('/update/{id}', [AdminExerciseSkillTestController::class, 'update'])->name('update');
                Route::delete('/delete/{id}', [AdminExerciseSkillTestController::class, 'destroy'])->name('destroy');
            });
        });
    });
});
