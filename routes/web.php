<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\JobController;
use App\Http\Controllers\User\Auth\LoginController;
use App\Http\Controllers\User\Auth\RegisterController;
use App\Http\Controllers\User\UserApplicationTestController;
use App\Http\Controllers\User\UserDashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Guest Route
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/job', [JobController::class, 'index'])->name('job');
Route::get('/job/detail/{id}', [JobController::class, 'show'])->name('job.detail');
Route::get('/job/filter/{id}', [JobController::class, 'filter'])->name('job.filter');
Route::get('/job/search', [JobController::class, 'search'])->name('job.search');
Route::get('/job/apply', [JobController::class, 'apply'])->name('job.apply');

Route::group(['middleware' => 'guest'], function () {
    Route::get('/login', [LoginController::class, 'index'])->name('login');
    Route::post('/login', [LoginController::class, 'authenticate'])->name('login.post');

    Route::get('/register', [RegisterController::class, 'index'])->name('register');
    Route::post('/register', [RegisterController::class, 'register'])->name('register.post');
});

Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

// User Route
Route::group([
    'prefix'         => 'user',
    'as'            => 'user.',
    'middleware'    => 'auth'
], function () {
    Route::get('/dashboard', [UserDashboardController::class, 'index'])->name('dashboard');
    Route::get('/job', [UserDashboardController::class, 'listJob'])->name('job');
    Route::get('/job/detail/{id}', [UserDashboardController::class, 'showJob'])->name('job.detail');
    Route::get('/job/filter/{id}', [UserDashboardController::class, 'filterJob'])->name('job.filter');
    Route::get('/job/search', [UserDashboardController::class, 'searchJob'])->name('job.search');
    Route::get('/job/apply/{job_id}', [UserDashboardController::class, 'applyJob'])->name('job.apply');
    Route::get('/job/apply/store/{user_id}/{job_id}', [UserDashboardController::class, 'storeApplyJob'])->name('job.apply.store');
    Route::get('/profile/{id}', [UserDashboardController::class, 'profile'])->name('profile');
    Route::post('/profile/basic-info/update/{id}', [UserDashboardController::class, 'updateBasicInfo'])->name('profile.basic-info.update');

    Route::post('/profile/education/store/{id}', [UserDashboardController::class, 'storeEducation'])->name('profile.education.store');
    Route::get('/profile/education/edit/{id}', [UserDashboardController::class, 'editEducation'])->name('profile.education.edit');
    Route::post('/profile/education/update/{id}', [UserDashboardController::class, 'updateEducation'])->name('profile.education.update');
    Route::delete('/profile/education/delete/{id}', [UserDashboardController::class, 'deleteEducation'])->name('profile.education.delete');

    Route::post('/profile/experience/store/{id}', [UserDashboardController::class, 'storeExperience'])->name('profile.experience.store');
    Route::get('/profile/experience/edit/{id}', [UserDashboardController::class, 'editExperience'])->name('profile.experience.edit');
    Route::post('/profile/experience/update/{id}', [UserDashboardController::class, 'updateExperience'])->name('profile.experience.update');
    Route::delete('/profile/experience/delete/{id}', [UserDashboardController::class, 'deleteExperience'])->name('profile.experience.delete');

    Route::post('/profile/document/store/{id}', [UserDashboardController::class, 'storeDocument'])->name('profile.document.store');
    Route::get('/profile/document/edit/{id}', [UserDashboardController::class, 'editDocument'])->name('profile.document.edit');
    Route::post('/profile/document/update/{id}', [UserDashboardController::class, 'updateDocument'])->name('profile.document.update');
    Route::delete('/profile/document/delete/{id}', [UserDashboardController::class, 'deleteDocument'])->name('profile.document.delete');

    Route::get('/exam/psychological/{applicant_id}', [UserApplicationTestController::class, 'psychological'])->name('exam.psychological');
    Route::post('/exam/psychological/store', [UserApplicationTestController::class, 'submitPsychological'])->name('exam.psychological.store');
    Route::get('/exam/skill/{applicant_id}', [UserApplicationTestController::class, 'skill'])->name('exam.skill');
    Route::post('/exam/skill/store', [UserApplicationTestController::class, 'submitSkill'])->name('exam.skill.store');

    Route::get('/exam', [UserDashboardController::class, 'exam'])->name('exam');
});

