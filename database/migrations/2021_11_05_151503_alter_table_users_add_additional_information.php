<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableUsersAddAdditionalInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('full_name')->nullable();
            $table->date('birth_date')->nullable();
            $table->enum('gender', ['Male', 'Female', 'Not Sure'])->default('Not Sure');
            $table->mediumText('address')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('hobby')->nullable();
            $table->string('image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('full_name');
            $table->dropColumn('birth_date');
            $table->dropColumn('gender');
            $table->dropColumn('address');
            $table->dropColumn('phone_number');
            $table->dropColumn('hobby');
            $table->dropColumn('image');
        });
    }
}
