<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacancyJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancy_jobs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('division_id')
                ->constrained('vacancy_divisions')
                ->onDelete('cascade');

            $table->string('job_name');
            $table->string('job_level');
            $table->string('job_location');
            $table->date('job_period_date');
            $table->text('job_short_description');
            $table->mediumText('job_description');
            $table->mediumText('job_requirement');
            $table->integer('job_count_applied')->default(0);
            $table->boolean('publish')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancy_jobs');
    }
}
