<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableUserApplicationsAddStatusTestColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_applications', function (Blueprint $table) {
            $table->boolean('isHavePsychologicalTest')->default(false);
            $table->boolean('isHaveSkillTest')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_applications', function (Blueprint $table) {
            $table->dropColumn('isHavePsychologicalTest');
            $table->dropColumn('isHaveSkillTest');
        });
    }
}
