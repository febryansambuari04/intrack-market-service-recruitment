<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPsychologicalTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_psychological_tests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_applicant_id')
                ->constrained('user_applications')
                ->onDelete('cascade');
            $table->foreignId('psychological_test_id')
                ->constrained('exercise_psychological_tests')
                ->onDelete('cascade');
            $table->string('answer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_psychological_tests');
    }
}
