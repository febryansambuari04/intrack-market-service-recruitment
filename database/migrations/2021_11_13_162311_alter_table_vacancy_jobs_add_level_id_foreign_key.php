<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableVacancyJobsAddLevelIdForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vacancy_jobs', function (Blueprint $table) {
            $table->foreignId('level_id')
                ->after('division_id')
                ->constrained('vacancy_levels')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vacancy_jobs', function (Blueprint $table) {
            $table->dropForeign('vacancy_jobs_level_id_foreign');
            $table->dropColumn('level_id');
        });
    }
}
