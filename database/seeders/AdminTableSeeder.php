<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => 'Administrator',
            'email' => 'admin@intrack.com',
            'password' => Hash::make('secret!2022'),
            'role'  => 'superadmin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
