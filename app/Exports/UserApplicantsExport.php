<?php

namespace App\Exports;

use App\Models\UserApplication;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class UserApplicantsExport implements FromCollection, WithHeadings, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('user_applications')
            ->join('users', 'user_applications.user_id', '=', 'users.id')
            ->join('vacancy_jobs', 'user_applications.job_id', '=', 'vacancy_jobs.id')
            ->select('users.name', 'user_applications.test_score', 'vacancy_jobs.job_name', 'user_applications.status')
            ->get();
    }

    public function headings(): array
    {
        return ["Name", "Score", "Job Name", "Status"];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            'A1:I50'  => [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                    ]
                ]
            ],
        ];
    }
}
