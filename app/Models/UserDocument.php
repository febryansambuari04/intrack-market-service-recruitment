<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDocument extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'document_name',
        'file'
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function scopeGetDataByUserId($query, $userId)
    {
        return $query->where('user_id', $userId);
    }

    public function scopeIsUserHaveDocument($query, $user)
    {
        return $query->whereBelongsTo($user);
    }
}
