<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSkillTest extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_applicant_id',
        'skill_test_id',
        'answer'
    ];

    public function userApplicant()
    {
        return $this->BelongsTo(UserApplication::class, 'user_applicant_id');
    }

    public function skillTest()
    {
        return $this->hasOne(ExerciseSkillTest::class, 'id', 'skill_test_id');
    }
}
