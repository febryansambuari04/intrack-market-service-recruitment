<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserExperience extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'company_name',
        'position_name',
        'position_level',
        'start_period',
        'end_period',
        'working_description'
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function scopeGetDataByUserId($query, $userId)
    {
        return $query->where('user_id', $userId);
    }

    public function scopeIsUserHaveExperience($query, $user)
    {
        return $query->whereBelongsTo($user);
    }
}
