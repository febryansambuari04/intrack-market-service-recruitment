<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExerciseSkillTest extends Model
{
    use HasFactory;

    protected $fillable = [
        'division_id',
        'question',
        'answer_1',
        'answer_2',
        'answer_3',
        'answer_4',
        'right_answer',
        'publish'
    ];

    protected $appends = [
        'division_name'
    ];

    public function division()
    {
        return $this->belongsTo(VacancyDivision::class, 'division_id');
    }

    public function getDivisionNameAttribute()
    {
        return $this->division ? $this->division->name : '';
    }

    public function scopePublished($query)
    {
        return $query->where('publish', 1);
    }
}
