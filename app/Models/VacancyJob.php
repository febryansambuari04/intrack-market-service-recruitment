<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VacancyJob extends Model
{
    use HasFactory;

    protected $fillable = [
        'division_id',
        'level_id',
        'job_name',
        'job_type',
        'job_location',
        'job_period_date',
        'job_short_description',
        'job_description',
        'job_requirement',
        'job_count_applied',
        'publish'
    ];

    protected $appends = [
        'division_name',
        'level_name',
        'job_type_name'
    ];

    public function division()
    {
        return $this->belongsTo(VacancyDivision::class, 'division_id');
    }

    public function level()
    {
        return $this->belongsTo(VacancyLevel::class, 'level_id');
    }

    public function getDivisionNameAttribute()
    {
        return $this->division ? $this->division->name : '';
    }

    public function getLevelNameAttribute()
    {
        return $this->level ? $this->level->name : '';
    }

    public function getJobTypeNameAttribute()
    {
        $jobType = "Internship";

        if ($this->job_type == "fulltime") {
            $jobType = "Full Time";
        } else if ($this->job_type == "parttime") {
            $jobType = "Part Time";
        } else if ($this->job_type == "freelance") {
            $jobType = "Freelance";
        }

        return $jobType;
    }

    public function scopePublished($query)
    {
        return $query->where('publish', 1);
    }
}
