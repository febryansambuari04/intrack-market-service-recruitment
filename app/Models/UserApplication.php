<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserApplication extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'job_id',
        'status',
        'zoom_url',
        'description',
        'test_score',
        'isHavePsychologicalTest',
        'isHaveSkillTest'
    ];

    protected $appends = [
        'user_name',
        'job_name'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function job()
    {
        return $this->belongsTo(VacancyJob::class, 'job_id');
    }

    public function psychologicalTest()
    {
        return $this->hasMany(UserPsychologicalTest::class, 'user_applicant_id');
    }

    public function skillTest()
    {
        return $this->hasMany(UserSkillTest::class, 'user_applicant_id');
    }

    public function scopeStatusWaiting($query)
    {
        return $query->where('status', 'waiting');
    }

    public function scopeStatusApplied($query)
    {
        return $query->where('status', 'applied');
    }

    public function scopeStatusAccepted($query)
    {
        return $query->where('status', 'accepted');
    }

    public function getUserNameAttribute()
    {
        return $this->user ? $this->user->name : '';
    }

    public function getJobNameAttribute()
    {
        return $this->job ? $this->job->job_name : '';
    }
}
