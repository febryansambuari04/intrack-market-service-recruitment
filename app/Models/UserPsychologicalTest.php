<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserPsychologicalTest extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_applicant_id',
        'psychological_test_id',
        'answer'
    ];

    public function userApplicant()
    {
        return $this->BelongsTo(UserApplication::class, 'user_applicant_id');
    }

    public function psychologicalTest()
    {
        return $this->hasOne(ExercisePsychologicalTest::class, 'id', 'psychological_test_id');
    }
}
