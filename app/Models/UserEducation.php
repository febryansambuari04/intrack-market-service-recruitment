<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserEducation extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'school_name',
        'degree',
        'major',
        'gpa',
        'gpa_max',
        'start_period',
        'end_period'
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function scopeGetDataByUserId($query, $userId)
    {
        return $query->where('user_id', $userId);
    }

    public function scopeIsUserHaveEducation($query, $user)
    {
        return $query->whereBelongsTo($user);
    }
}
