<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VacancyDivision extends Model
{
    use HasFactory;

    protected $fillable = [
        'icon',
        'name',
        'description'
    ];

    public function jobs()
    {
        return $this->hasMany(VacancyJob::class, 'division_id');
    }

    public function getDivisionIconLinkAttribute()
    {
        if ($this->icon) {
            return asset('image/admin/vacancy/division/icon/' . $this->icon);
        } else {
            return null;
        }
    }
}
