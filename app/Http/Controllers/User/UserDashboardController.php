<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserApplication;
use App\Models\UserDocument;
use App\Models\UserEducation;
use App\Models\UserExperience;
use App\Models\VacancyDivision;
use App\Models\VacancyJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class UserDashboardController extends Controller
{
    public function index()
    {
        $jobs = VacancyJob::published()
            ->take(3)
            ->get();

        $userId = auth()->user()->id;

        $userApplications = UserApplication::where('user_id', $userId)
            ->with(['user', 'job', 'psychologicalTest'])
            ->orderBy('created_at', 'asc')
            ->get();

        $userApplicationWaiting = UserApplication::where('user_id', $userId)
            ->statusWaiting()
            ->first();

        $userApplicationApplied = UserApplication::where('user_id', $userId)
            ->statusApplied()
            ->first();

        $userApplicationAccepted = UserApplication::where('user_id', $userId)
            ->statusAccepted()
            ->first();

        return view('public.user.dashboard.index', compact('jobs', 'userApplicationWaiting', 'userApplications', 'userApplicationApplied', 'userApplicationAccepted'));
    }

    public function listJob()
    {
        $divisions = VacancyDivision::with(['jobs' => function ($qJobs) {
            return $qJobs->published();
        }])
        ->orderBy('name', 'desc')
        ->get();

        return view('public.user.job.job-list', compact('divisions'));
    }

    public function showJob($id)
    {
        $job = VacancyJob::find($id);

        return Response::json($job);
    }

    public function filterJob($id)
    {
        $divisions = VacancyDivision::with(['jobs' => function ($qJobs) use ($id) {
            return $qJobs->where('division_id', $id)
                ->published();
        }])
        ->orderBy('name', 'desc')
        ->get();

        return view('public.user.job.job-list', compact('divisions'));
    }

    public function searchJob(Request $request)
    {
        $q = $request->get('q');

        $qDivisions = VacancyDivision::with(['jobs' => function ($qJobs) use ($q) {
            return $qJobs->published()
                ->where('job_name', 'LIKE', '%' . $q . '%')
                ->orWhere('job_type', 'LIKE', '%' . $q . '%')
                ->orWhere('job_location', 'LIKE', '%' . $q . '%');
        }]);

        $divisions = $qDivisions->orderBy('name', 'desc')
            ->paginate(10);

        if ($q) {
            $divisions->withPath(route('user.job') . "?q={$q}");
        } else {
            $divisions->withPath(route('user.job'));
        }

        return view('public.user.job.job-list', get_defined_vars());
    }

    public function applyJob(Request $request)
    {
        $jobId = $request->job_id;

        $job = VacancyJob::with('division')
            ->findOrFail($jobId);

        $userId = auth()->user()->id;

        $userApplied = UserApplication::where('user_id', $userId)
            ->where('job_id', $jobId)
            ->where('status', 'applied')
            ->orWhere('status', 'accepted')
            ->first();

        $isUserFilledData = $this->checkingUserData(auth()->user());

        return view('public.user.job.apply', compact('job', 'userApplied', 'isUserFilledData'));
    }

    private function checkingUserData($user): bool
    {
        $result = true;

        $userEducation = UserEducation::isUserHaveEducation($user)->get();
        $userExperience = UserExperience::isUserHaveExperience($user)->get();
        $userDocument = UserDocument::isUserHaveDocument($user)->get();

        if ($userEducation->isEmpty() || $userExperience->isEmpty() || $userDocument->isEmpty()) {
            $result = false;
        }

        return $result;
    }

    public function storeApplyJob(Request $request)
    {
        $userApplication = new UserApplication();
        $userApplication->user_id = $request->user_id;
        $userApplication->job_id = $request->job_id;
        $userApplication->status = "waiting";
        $userApplication->save();

        $job = VacancyJob::with('division')
            ->findOrFail($request->job_id);

        return view('public.user.job.apply-success', compact('job'));
    }

    public function profile($id)
    {
        $user = User::findOrFail($id);

        $userEducations = User::find($id)->educations;

        $userExperiences = User::find($id)->experiences;

        $userDocuments = User::find($id)->documents;

        return view('public.user.profile.index', compact('user', 'userEducations', 'userExperiences', 'userDocuments'));
    }

    public function updateBasicInfo(Request $request, $id)
    {
        $request->validate([
            'image'         => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'full_name'     => 'required',
            'name'          => 'required',
            'email'         => 'required|unique:users,email,'.$id,
            'birth_date'    => 'required',
            'gender'        => 'required',
            'address'       => 'required',
            'phone_number'  => 'required',
            'hobby'         => 'required'
        ]);

        $user = User::findOrFail($id);

        if ($image = $request->file('image')) {
            $destinationPath = 'image/user/profile/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);

            $user->update([
                'full_name'     => $request->full_name,
                'name'          => $request->name,
                'email'         => $request->email,
                'birth_date'    => $request->birth_date,
                'gender'        => $request->gender,
                'address'       => $request->address,
                'phone_number'  => $request->phone_number,
                'hobby'         => $request->hobby,
                'image'         => $profileImage
            ]);
        } else {
            $user->update([
                'full_name'     => $request->full_name,
                'name'          => $request->name,
                'email'         => $request->email,
                'birth_date'    => $request->birth_date,
                'gender'        => $request->gender,
                'address'       => $request->address,
                'phone_number'  => $request->phone_number,
                'hobby'         => $request->hobby
            ]);
        }

        if ($user) {
            return redirect()
                ->back()
                ->with('success', 'Basic Information updated successfully');
        } else {
            return redirect()
                ->back()
                ->with('errors', 'Basic Information updated failed');
        }
    }

    public function storeEducation(Request $request, $id)
    {
        $request->validate([
            'school_name'   => 'required',
            'degree'        => 'required',
            'major'         => 'required',
            'gpa'           => 'required|between:0,99.99',
            'gpa_max'       => 'required|between:0,99.99',
            'start_period'  => 'required',
            'end_period'    => 'required'
        ]);

        UserEducation::create($request->all());

        return redirect()
            ->back()
            ->with('success', 'Education created successfully');
    }

    public function editEducation($id)
    {
        $education = UserEducation::find($id)->first();

        return Response::json($education);
    }

    public function updateEducation(Request $request)
    {
        $request->validate([
            'school_name'   => 'required',
            'degree'        => 'required',
            'major'         => 'required',
            'gpa'           => 'required|between:0,99.99',
            'gpa_max'       => 'required|between:0,99.99',
            'start_period'  => 'required',
            'end_period'    => 'required'
        ]);

        $userEducation = UserEducation::find($request->id);
        $userEducation->school_name = $request->school_name;
        $userEducation->degree = $request->degree;
        $userEducation->major = $request->major;
        $userEducation->gpa = $request->gpa;
        $userEducation->gpa_max = $request->gpa_max;
        $userEducation->start_period = $request->start_period;
        $userEducation->end_period = $request->end_period;
        $userEducation->save();

        $request->session()->flash('success', 'Education updated successfully');

        return response()->json(['success' => 'Education updated successfully']);
    }

    public function deleteEducation($id)
    {
        UserEducation::find($id)->delete();

        return redirect()
            ->back()
            ->with('success', 'Education deleted successfully');
    }

    public function storeExperience(Request $request, $id)
    {
        $request->validate([
            'company_name'          => 'required',
            'position_name'         => 'required',
            'position_level'        => 'required',
            'start_period'          => 'required',
            'end_period'            => 'required',
            'working_description'   => 'required'
        ]);

        UserExperience::create($request->all());

        return redirect()
            ->back()
            ->with('success', 'Experience created successfully');
    }

    public function editExperience($id)
    {
        $experience = UserExperience::find($id)->first();

        return Response::json($experience);
    }

    public function updateExperience(Request $request)
    {
        $request->validate([
            'company_name'          => 'required',
            'position_name'         => 'required',
            'position_level'        => 'required',
            'start_period'          => 'required',
            'end_period'            => 'required',
            'working_description'   => 'required'
        ]);

        $userExperience = UserExperience::find($request->id);
        $userExperience->company_name = $request->company_name;
        $userExperience->position_name = $request->position_name;
        $userExperience->position_level = $request->position_level;
        $userExperience->start_period = $request->start_period;
        $userExperience->end_period = $request->end_period;
        $userExperience->working_description = $request->working_description;
        $userExperience->save();

        $request->session()->flash('success', 'Experience updated successfully');

        return response()->json(['success' => 'Experience updated successfully']);
    }

    public function deleteExperience($id)
    {
        UserExperience::find($id)->delete();

        return redirect()
            ->back()
            ->with('success', 'Experience deleted successfully');
    }

    public function storeDocument(Request $request)
    {
        $request->validate([
            'document_name'    => 'required',
            'file'              => 'required|mimes:pdf,png,jpg|max:2048'
        ]);

        $document = new UserDocument();
        $document->fill($request->all());

        if ($file = $request->file('file')) {
            $destinationPath = 'file/user/profile/';
            $fileDocument = date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $fileDocument);

            $document->file = $fileDocument;
        }

        $document->save();

        return redirect()
            ->back()
            ->with('success', 'Document created successfully');
    }

    public function editDocument($id)
    {
        $document = UserDocument::find($id)->first();

        return Response::json($document);
    }

    public function deleteDocument($id)
    {
        UserDocument::findOrFail($id)->delete();

        return redirect()
            ->back()
            ->with('success', 'Document deleted successfully');
    }

    public function exam()
    {
        return view('public.user.online-test.online-test');
    }
}
