<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\ExercisePsychologicalTest;
use App\Models\ExerciseSkillTest;
use App\Models\UserApplication;
use App\Models\UserPsychologicalTest;
use App\Models\UserSkillTest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserApplicationTestController extends Controller
{
    public function psychological($applicant_id)
    {
        $psychological = ExercisePsychologicalTest::published()
            ->inRandomOrder()
            ->limit(10)
            ->get();

        return view('public.user.online-test.psychological', compact('applicant_id', 'psychological'));
    }

    public function submitPsychological(Request $request)
    {
        foreach ($request->psychological_test_id as $idx => $psychological) {
            $psychologicalTest = new UserPsychologicalTest();
            $psychologicalTest->user_applicant_id = $request->user_applicant_id;
            $psychologicalTest->psychological_test_id = $psychological;
            $psychologicalTest->answer = $request->answer_[$psychological] ?? null;
            $psychologicalTest->save();
        }

        $userApplicant = UserApplication::findOrFail($request->user_applicant_id);
        $userApplicant->isHavePsychologicalTest = true;
        $userApplicant->save();

        return redirect()
            ->back()
            ->with('psychologicalSuccessMessage', 'Success');
    }

    public function skill($applicant_id)
    {
        $skills = ExerciseSkillTest::published()
            ->inRandomOrder()
            ->limit(10)
            ->get();

        return view('public.user.online-test.skill', compact('applicant_id', 'skills'));
    }

    public function submitSkill(Request $request)
    {
        foreach ($request->skill_test_id as $idx => $skill) {
            $skillTest = new UserSkillTest();
            $skillTest->user_applicant_id = $request->user_applicant_id;
            $skillTest->skill_test_id = $skill;
            $skillTest->answer = $request->answer_[$idx+1] ?? null;
            $skillTest->save();
        }

        $skills = UserSkillTest::where('user_applicant_id', $request->user_applicant_id)
            ->get();

        $rightAnswer = 0;
        foreach ($skills as $skill) {
            $skillExercise = ExerciseSkillTest::findOrFail($skill->skill_test_id);
            if (Str::lower($skill->answer) === Str::lower($skillExercise->right_answer)) {
                $rightAnswer += 10;
            }

            $userApplicant = UserApplication::findOrFail($skill->user_applicant_id);
            $userApplicant->test_score = $rightAnswer;
            $userApplicant->isHaveSkillTest = true;
            $userApplicant->save();
        }

        return redirect()
            ->back()
            ->with('skillSuccessMessage', 'Success');
    }
}
