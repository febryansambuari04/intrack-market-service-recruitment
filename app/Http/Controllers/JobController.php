<?php

namespace App\Http\Controllers;

use App\Models\VacancyDivision;
use App\Models\VacancyJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class JobController extends Controller
{
    public function index()
    {
        $divisions = VacancyDivision::with(['jobs' => function ($qJobs) {
            return $qJobs->published();
        }])
        ->orderBy('name', 'desc')
        ->get();

        return view('public.guest.job', compact('divisions'));
    }

    public function show($id)
    {
        $job = VacancyJob::find($id);

        return Response::json($job);
    }

    public function filter($id)
    {
        $divisions = VacancyDivision::with(['jobs' => function ($qJobs) use ($id) {
            return $qJobs->where('division_id', $id)
                ->published();
        }])
        ->orderBy('name', 'desc')
        ->get();

        return view('public.guest.job', compact('divisions'));
    }

    public function search(Request $request)
    {
        $q = $request->get('q');

        $qDivisions = VacancyDivision::with(['jobs' => function ($qJobs) use ($q) {
            return $qJobs->published()
                ->where('job_name', 'LIKE', '%' . $q . '%')
                ->orWhere('job_type', 'LIKE', '%' . $q . '%')
                ->orWhere('job_location', 'LIKE', '%' . $q . '%');
        }]);

        $divisions = $qDivisions->orderBy('name', 'desc')
            ->paginate(10);

        if ($q) {
            $divisions->withPath(route('job') . "?q={$q}");
        } else {
            $divisions->withPath(route('job'));
        }

        return view('public.guest.job', get_defined_vars());
    }

    public function apply()
    {
        if (!Auth::check()) {
            $message = "You should login to apply this job!";

            return Response::json($message);
        }
    }
}
