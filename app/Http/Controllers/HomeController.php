<?php

namespace App\Http\Controllers;

use App\Models\VacancyDivision;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $divisions = VacancyDivision::orderBy('name', 'desc')
            ->withCount('jobs')
            ->get();

        return view('public.guest.index', compact('divisions'));
    }

    public function job()
    {
        return view('public.guest.job');
    }
}
