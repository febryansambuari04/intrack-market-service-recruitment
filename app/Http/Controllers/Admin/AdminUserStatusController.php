<?php

namespace App\Http\Controllers\Admin;

use App\Exports\UserApplicantsExport;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserApplication;
use App\Models\UserPsychologicalTest;
use App\Models\UserSkillTest;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class AdminUserStatusController extends Controller
{
    public function index()
    {
        $applicants = UserApplication::with(['user', 'job', 'psychologicalTest', 'skillTest'])
            ->orderBy('updated_at', 'desc')
            ->get();

        return view('admin.user.status.index', compact('applicants'));
    }

    public function edit($id)
    {
        $applicant = UserApplication::findOrFail($id);

        return view('admin.user.status.edit', compact('applicant'));
    }

    public function update(Request $request, $id)
    {
        $applicant = UserApplication::findOrFail($id);
        $applicant->status = $request->status;
        $applicant->zoom_url = $request->zoom_url ?? null;
        $applicant->description = $request->description ?? null;
        $applicant->save();

        return redirect()
            ->route('admin.user.status.index')
            ->with('statusApplicantSuccessMessage', 'Status has been updated');
    }

    public function delete($id)
    {
        UserApplication::find($id)->delete();

        return redirect()
            ->back()
            ->with('statusApplicantSuccessMessage', 'Status deleted successfully');
    }

    public function exportUserStatus()
    {
        return Excel::download(new UserApplicantsExport, 'users-applicant-status.xlsx');
    }

    public function viewTest($id)
    {
        $applicantTest = UserApplication::findOrFail($id);

        $psychologicalTest = UserPsychologicalTest::where('user_applicant_id', $applicantTest->id)
            ->get();

        $skillTest = UserSkillTest::where('user_applicant_id', $applicantTest->id)
            ->get();

        return view('admin.user.status.view-test', get_defined_vars());
    }

    public function viewData($id)
    {
        $applicant = UserApplication::findOrFail($id);

        $user = User::where('id', $applicant->user_id)
            ->with(['educations', 'experiences', 'documents'])
            ->first();

        return view('admin.user.status.view-data', compact('user'));
    }
}
