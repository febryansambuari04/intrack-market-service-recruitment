<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\VacancyDivision;
use Illuminate\Http\Request;

class AdminVacancyDivisionController extends Controller
{
    public function index()
    {
        $divisions = VacancyDivision::orderBy('name', 'desc')
            ->paginate(6);

        return view('admin.vacancy.division.index', compact('divisions'));
    }

    public function create()
    {
        return view('admin.vacancy.division.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'icon'          => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name'          => 'required',
            'description'   => 'required'
        ]);

        $input = $request->all();

        if ($icon = $request->file('icon')) {
            $destinationPath = 'image/admin/vacancy/division/icon/';
            $iconName = date('YmdHis') . "." . $icon->getClientOriginalExtension();
            $icon->move($destinationPath, $iconName);
            $input['icon'] = "$iconName";
        }

        VacancyDivision::create($input);

        return redirect()
            ->route('admin.vacancy.division.index')
            ->with('vacancyDivisionMessage', 'Division created successfully');
    }

    public function edit($id)
    {
        $division = VacancyDivision::findOrFail($id);

        return view('admin.vacancy.division.edit', compact('division'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'icon'          => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name'          => 'required',
            'description'   => 'required'
        ]);

        $input = $request->all();

        if ($icon = $request->file('icon')) {
            $destinationPath = 'image/admin/vacancy/division/icon/';
            $iconName = date('YmdHis') . "." . $icon->getClientOriginalExtension();
            $icon->move($destinationPath, $iconName);
            $input['icon'] = "$iconName";
        } else {
            unset($input['icon']);
        }

        $division = VacancyDivision::findOrFail($id);
        $division->update($input);

        return redirect()
            ->route('admin.vacancy.division.index')
            ->with('vacancyDivisionMessage', 'Division updated successfully');
    }

    public function destroy($id)
    {
        $division = VacancyDivision::findOrFail($id);

        if ($division->delete()) {
            unlink("image/admin/vacancy/division/icon/" . $division->icon);
        }

        return redirect()
            ->back()
            ->with('vacancyDivisionMessage', 'Division deleted successfully');
    }
}
