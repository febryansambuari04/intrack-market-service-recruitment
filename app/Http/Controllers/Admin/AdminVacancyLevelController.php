<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\VacancyLevel;
use Illuminate\Http\Request;

class AdminVacancyLevelController extends Controller
{
    public function index()
    {
        $levels = VacancyLevel::orderBy('name', 'desc')
            ->get();

        return view('admin.vacancy.level.index', compact('levels'));
    }

    public function create()
    {
        return view('admin.vacancy.level.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required'
        ]);

        $input = $request->all();

        VacancyLevel::create($input);

        return redirect()
            ->route('admin.vacancy.level.index')
            ->with('vacancyLevelMessage', 'Level created successfully');
    }

    public function edit($id)
    {
        $level = VacancyLevel::findOrFail($id);

        return view('admin.vacancy.level.edit', compact('level'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => 'required'
        ]);

        $input = $request->all();

        $level = VacancyLevel::findOrFail($id);
        $level->update($input);

        return redirect()
            ->route('admin.vacancy.level.index')
            ->with('vacancyLevelMessage', 'Level updated successfully');
    }

    public function destroy($id)
    {
        VacancyLevel::findOrFail($id)->delete();

        return redirect()
            ->back()
            ->with('vacancyLevelMessage', 'Level deleted successfully');
    }
}
