<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ExerciseSkillTest;
use App\Models\VacancyDivision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class AdminExerciseSkillTestController extends Controller
{
    public function index()
    {
        $skills = ExerciseSkillTest::orderBy('updated_at', 'desc')
            ->paginate(10);

        return view('admin.exercise.skill.index', compact('skills'));
    }

    public function create()
    {
        $divisions = VacancyDivision::orderBy('name', 'desc')
            ->get();

        return view('admin.exercise.skill.create', compact('divisions'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'division_id'   => 'required',
            'question'      => 'required',
            'answer_1'      => 'required',
            'answer_2'      => 'required',
            'answer_3'      => 'required',
            'answer_4'      => 'required',
            'right_answer'  => 'required',
            'publish'       => 'required|boolean'
        ]);

        $input = $request->all();

        ExerciseSkillTest::create($input);

        return redirect()
            ->route('admin.exercise.skill.index')
            ->with('skillTestMessage', 'Question created successfully');
    }

    public function show($id)
    {
        $skill = ExerciseSkillTest::find($id);

        return Response::json($skill);
    }

    public function edit($id)
    {
        $skill = ExerciseSkillTest::findOrFail($id);

        $divisions = VacancyDivision::orderBy('name', 'desc')
            ->get();

        return view('admin.exercise.skill.edit', compact('skill', 'divisions'));
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'division_id'   => 'required',
            'question'      => 'required',
            'answer_1'      => 'required',
            'answer_2'      => 'required',
            'answer_3'      => 'required',
            'answer_4'      => 'required',
            'right_answer'  => 'required',
            'publish'       => 'required|boolean'
        ]);

        $input = $request->all();

        $skill = ExerciseSkillTest::findOrFail($id);
        $skill->update($input);

        return redirect()
            ->route('admin.exercise.skill.index')
            ->with('skillTestMessage', 'Question updated successfully');
    }

    public function destroy($id)
    {
        ExerciseSkillTest::findOrFail($id)->delete();

        return redirect()
            ->back()
            ->with('skillTestMessage', 'Question deleted successfully');
    }
}
