<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ExercisePsychologicalTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class AdminExercisePsychologicalTestController extends Controller
{
    public function index()
    {
        $psychological = ExercisePsychologicalTest::orderBy('updated_at', 'desc')
            ->paginate(10);

        return view('admin.exercise.psychological.index', compact('psychological'));
    }

    public function create()
    {
        return view('admin.exercise.psychological.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'question'  => 'required',
            'answer_1'  => 'required',
            'answer_2'  => 'required',
            'answer_3'  => 'required',
            'answer_4'  => 'required',
            'publish'
        ]);

        $input = $request->all();

        ExercisePsychologicalTest::create($input);

        return redirect()
            ->route('admin.exercise.psychological.index')
            ->with('psychologicalTestMessage', 'Question created successfully');
    }

    public function show($id)
    {
        $psychological = ExercisePsychologicalTest::find($id);

        return Response::json($psychological);
    }

    public function edit($id)
    {
        $psychological = ExercisePsychologicalTest::findOrFail($id);

        return view('admin.exercise.psychological.edit', compact('psychological'));
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'question'  => 'required',
            'answer_1'  => 'required',
            'answer_2'  => 'required',
            'answer_3'  => 'required',
            'answer_4'  => 'required',
            'publish'
        ]);

        $input = $request->all();

        $psychological = ExercisePsychologicalTest::findOrFail($id);
        $psychological->update($input);

        return redirect()
            ->route('admin.exercise.psychological.index')
            ->with('psychologicalTestMessage', 'Question updated successfully');
    }

    public function destroy($id)
    {
        ExercisePsychologicalTest::findOrFail($id)->delete();

        return redirect()
            ->back()
            ->with('psychologicalTestMessage', 'Question deleted successfully');
    }
}
