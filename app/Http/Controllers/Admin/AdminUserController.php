<?php

namespace App\Http\Controllers\Admin;

use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserEducation;
use App\Models\UserExperience;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Facades\Excel;

class AdminUserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('created_at', 'desc')
            ->withCount('educations')
            ->withCount('experiences')
            ->withCount('documents')
            ->paginate(10);

        return view('admin.user.index', compact('users'));
    }

    public function show($id)
    {
        $user = User::find($id);

        return Response::json($user);
    }

    public function deleteUser($id)
    {
        // Delete relationship with user first
        $userEducation = UserEducation::getDataByUserId($id)->get();
        if (count($userEducation) > 0) {
            $userEducation->delete();
        }

        $userExperience = UserExperience::getDataByUserId($id)->get();
        if (count($userExperience) > 0) {
            $userExperience->delete();
        }

        User::find($id)->delete();

        return redirect()
            ->back()
            ->with('userDeleteSuccess', 'User deleted successfully');
    }

    public function exportUser()
    {
        return Excel::download(new UsersExport, 'users-applicant.xlsx');
    }
}
