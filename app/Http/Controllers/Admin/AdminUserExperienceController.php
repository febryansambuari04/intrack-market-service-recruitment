<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserExperience;
use Illuminate\Http\Request;

class AdminUserExperienceController extends Controller
{
    public function index($user_id)
    {
        $experiences = UserExperience::getDataByUserId($user_id)
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('admin.user.experience.index', compact('experiences'));
    }

    public function deleteEducation($id)
    {
        UserExperience::find($id)->delete();

        return redirect()
            ->back()
            ->with('experienceDeleteSuccess', 'User Experience deleted successfully');
    }
}
