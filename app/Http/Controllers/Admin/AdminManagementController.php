<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminManagementController extends Controller
{
    public function index()
    {
        $admins = Admin::all();

        return view('admin.management.index', compact('admins'));
    }

    public function create()
    {
        return view('admin.management.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required',
            'email'     => 'required|email',
            'password'  => 'required',
            'role'      => 'required'
        ]);

        $admin = new Admin();
        $admin->fill($request->all());
        $admin->password = Hash::make($request->password);
        $admin->save();

        return redirect()
            ->route('admin.management.index')
            ->with('adminManagementMessage', 'Admin created successfully');
    }

    public function destroy($id)
    {
        Admin::findOrFail($id)->delete();

        return redirect()
            ->back()
            ->with('adminManagementMessage', 'Admin deleted successfully');
    }
}
