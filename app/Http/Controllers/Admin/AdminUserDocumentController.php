<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserDocument;
use Illuminate\Http\Request;

class AdminUserDocumentController extends Controller
{
    public function index($user_id)
    {
        $documents = UserDocument::getDataByUserId($user_id)
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('admin.user.document.index', compact('documents'));
    }

    public function deleteDocument($id)
    {
        UserDocument::find($id)->delete();

        return redirect()
            ->back()
            ->with('documentDeleteSuccess', 'User Document deleted successfully');
    }
}
