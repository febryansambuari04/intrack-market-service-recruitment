<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ExercisePsychologicalTest;
use App\Models\ExerciseSkillTest;
use App\Models\User;
use App\Models\VacancyDivision;
use App\Models\VacancyJob;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class AdminDashboardController extends Controller
{
    public function index()
    {
        $countUsers = User::all()->count();

        $countDivisions = VacancyDivision::all()->count();

        $countJobs = VacancyJob::all()->count();

        $countPsychologicalTest = ExercisePsychologicalTest::all()->count();

        $countSkillTest = ExerciseSkillTest::all()->count();

        return view('admin.home.dashboard', get_defined_vars());
    }

    public function profile()
    {
        $admin = auth()->guard('admin')->user();

        return view('admin.home.profile', get_defined_vars());
    }

    public function updateProfile(Request $request)
    {
        $admin = auth()->guard('admin')->user();
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->save();

        return redirect()
            ->back()
            ->with('updateAdminProfileMessage', 'Profile successfully updated');
    }

    public function updateProfilePassword(Request $request)
    {
        $admin = auth()->guard('admin')->user();

        $adminPassword = $admin->password;

        $request->validate([
            'current_password'     => 'required',
            'new_password'         => 'required|same:confirm_new_password',
            'confirm_new_password'  => 'required'
        ]);

        if (!Hash::check($request->current_password, $adminPassword)) {
            return back()->with('current_password', 'Current Password not match');
        }

        $admin->password = Hash::make($request->new_password);
        $admin->save();

        return redirect()
            ->back()
            ->with('updateAdminProfileMessage', 'Password successfully updated');
    }
}
