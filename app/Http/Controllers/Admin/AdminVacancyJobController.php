<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\VacancyDivision;
use App\Models\VacancyJob;
use App\Models\VacancyLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class AdminVacancyJobController extends Controller
{
    public function index()
    {
        $jobs = VacancyJob::orderBy('job_name', 'desc')
            ->paginate(6);

        return view('admin.vacancy.job.index', compact('jobs'));
    }

    public function create()
    {
        $divisions = VacancyDivision::orderBy('name')
            ->get();

        $levels = VacancyLevel::orderBy('name')
            ->get();

        return view('admin.vacancy.job.create', compact('divisions', 'levels'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'division_id'           => 'required',
            'level_id'              => 'required',
            'job_name'              => 'required',
            'job_type'              => 'required',
            'job_location'          => 'required',
            'job_period_date'       => 'required',
            'job_short_description' => 'required',
            'job_description'       => 'required',
            'job_requirement'       => 'required',
            'publish'               => 'required|boolean'
        ]);

        $input = $request->all();

        VacancyJob::create($input);

        return redirect()
            ->route('admin.vacancy.job.index')
            ->with('vacancyJobMessage', 'Job created successfully');
    }

    public function show($id)
    {
        $job = VacancyJob::find($id);

        return Response::json($job);
    }

    public function edit($id)
    {
        $divisions = VacancyDivision::orderBy('name')
            ->get();

        $levels = VacancyLevel::orderBy('name')
            ->get();

        $job = VacancyJob::findOrFail($id);

        return view('admin.vacancy.job.edit', compact('divisions', 'levels', 'job'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'division_id'           => 'required',
            'level_id'              => 'required',
            'job_name'              => 'required',
            'job_type'              => 'required',
            'job_location'          => 'required',
            'job_period_date'       => 'required',
            'job_short_description' => 'required',
            'job_description'       => 'required',
            'job_requirement'       => 'required',
            'publish'               => 'required|boolean'
        ]);

        $input = $request->all();

        $job = VacancyJob::findOrFail($id);
        $job->update($input);

        return redirect()
            ->route('admin.vacancy.job.index')
            ->with('vacancyJobMessage', 'Job updated successfully');
    }

    public function destroy($id)
    {
        VacancyJob::findOrFail($id)->delete();

        return redirect()
            ->back()
            ->with('vacancyJobMessage', 'Job deleted successfully');
    }
}
