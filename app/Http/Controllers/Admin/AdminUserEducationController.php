<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserEducation;
use Illuminate\Http\Request;

class AdminUserEducationController extends Controller
{
    public function index($user_id)
    {
        $educations = UserEducation::getDataByUserId($user_id)
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('admin.user.education.index', compact('educations'));
    }

    public function deleteEducation($id)
    {
        UserEducation::find($id)->delete();

        return redirect()
            ->back()
            ->with('educationDeleteSuccess', 'User Education deleted successfully');
    }
}
